package cc.geek8.star.Entity;


import com.sun.istack.NotNull;

import cc.geek8.core.common.base.BaseModel;

public class UserInfo extends BaseModel {

    private int id;
    private String username;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
