package cc.geek8.star.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cc.geek8.star.Dao.UserInfoDao;

@Service
public class UserInfoService {
    @Autowired
    UserInfoDao userInfoDao;

    //新增记录返回自增id
    public int addRecord(String username, String password){
        return  userInfoDao.addRecord(username,password);
    }
}
