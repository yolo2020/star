package cc.geek8.star.Controller;


import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import cc.geek8.star.Service.UserInfoService;
import cc.geek8.star.Util.RedisOperator;
import cc.geek8.star.Util.JsonUtils;
import cc.geek8.star.Entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/userinfo")
@ResponseBody
public class UserInfoController {
    @Autowired
    UserInfoService userInfoService;

    @Autowired
    private StringRedisTemplate strRedis;

    @Autowired
    private RedisOperator redis;

    @RequestMapping("/test")
    public int test() {
        UserInfo user = new UserInfo();
        //user.setId("100111");
       user.setUsername("spring boot");
       user.setPassword("abc123");

        strRedis.opsForValue().set("json:user", JsonUtils.objectToJson(user));
        strRedis.opsForValue().set("username1","wangxinli");

        return userInfoService.addRecord("spring boot","abc123");
    }

    //URL举例:  http://localhost:8095/userinfo/addrecord/222/333
    @ApiOperation(value = "新增记录", httpMethod = "POST")
    @RequestMapping(value = "/addrecord/{username}/{password}")
    public int addRecord(@PathVariable("username") String username, @PathVariable("password") String password){
        //System.out.println("controller--request:user-register>>"+username+" "+password);
        return  userInfoService.addRecord(username,password);
    }
}
