package cc.geek8.star.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BaseController {
    @RequestMapping("/")
    @ResponseBody
    public String hello (){
        return "Hello MOTO";
    }


}
