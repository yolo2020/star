package cc.geek8.star.Dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserInfoDao {
    //新增记录返回自增id
    int addRecord(@Param("username") String username, @Param("password") String password);

}