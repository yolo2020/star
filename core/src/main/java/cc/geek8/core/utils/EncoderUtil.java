package cc.geek8.core.utils;


import java.nio.charset.Charset;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

public class EncoderUtil {

    /**
     * Base64加密
     *
     * @author WanWei
     * @param param
     * @return
     * @date 2016-10-13 下午1:42:52
     */
    public static String encode(String param) {
        if (param == null) {
            return null;
        }
        return Base64.encodeBase64String(param.getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Base64解密
     * @author WanWei
     * @param param
     * @return
     * @date 2015-10-13 下午1:42:52
     */
    public static String decode(String param) {
        if (param == null) {
            return null;
        }
        byte[] decode = Base64.decodeBase64(param.getBytes(Charset.forName("UTF-8")));
        return new String(decode);
    }

    /**
     * 获取随机密码
     * @param length
     * @return
     * @author WanWei
     * @date 2017年6月27日 下午2:21:52
     */
    public static String getRandomPassword(Integer length) {
        if (length == null) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            boolean b = random.nextBoolean();
            if (b) { // 字符串
                str.append((char) (97 + random.nextInt(26)));// 取得小写字母
            } else { // 数字
                str.append(String.valueOf(random.nextInt(10)));
            }
        }
        return str.toString();
    }
}
