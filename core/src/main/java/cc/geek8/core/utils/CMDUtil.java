package cc.geek8.core.utils;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 执行操作系统命令的工具类
 *
 * @author WanWei
 * @date 2016-3-30 上午10:23:18
 */
public class CMDUtil {

    private static Logger log = LoggerFactory.getLogger(CMDUtil.class);

    //代表操作系统的系统变量
    private final static String SYSTEM_OS_NAME = "os.name";

    //Linux操作系统
    private final static String OS_LINUX = "linux";

    //Windows操作系统
    private final static String OS_WINDOWS = "windows";

    //获取当前操作系统类型
    private static String os = System.getProperty(SYSTEM_OS_NAME).toLowerCase();

    public static  Process handle = null;

    /**
     * 判断当前是否为Linux操作系统
     *
     * @return
     * @author WanWei
     * @date 2016-3-30 上午10:42:32
     */
    private static boolean isLinux() {
        return os.indexOf(OS_LINUX) >= 0;
    }

    /**
     * 判断当前是否为Windows操作系统
     *
     * @return
     * @author WanWei
     * @date 2016-3-30 上午10:43:11
     */
    private static boolean isWindows() {
        return os.indexOf(OS_WINDOWS) >= 0;
    }

    /**
     * 执行系统命令
     *
     * @param cmdStr
     * @author WanWei
     * @date 2016-3-30 上午10:26:10
     */
    public static void exeCmd(String cmdStr) {
        Runtime rt = Runtime.getRuntime();
        Process p = null;
        LineNumberReader br = null;
        try {
            if(isLinux()) {
                String[] cmd = {"/bin/sh", "-c", cmdStr};
                p = Runtime.getRuntime().exec(cmd);
            } else if(isWindows()) {
                String cmd = "cmd /c ".concat(cmdStr);
                p = rt.exec(cmd);
            } else {
                log.error("系统不支持该操作系统[{}], 系统将会退出", os);
                System.exit(1);
            }
            br = new LineNumberReader(new InputStreamReader(p.getInputStream()));
            handle = p;
            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                log.info(line);
                sb.append(line).append("\n");
            }

        } catch(Exception e) {
            log.error("执行系统命令失败,系统将会退出", e);
            System.exit(1);
        } finally {
            if(br != null){
                try{
                    br.close();
                }catch(Exception e){
                    //do nothing
                    log.error(e.getMessage(), e);
                }
            }
        }

    }

}
