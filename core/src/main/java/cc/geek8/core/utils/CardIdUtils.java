package cc.geek8.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zhy
 * @date 2018/12/1 10:43
 */
public class CardIdUtils {

    // 验证港澳通行证
    public static final String REGEX_ID_HKMT = "^[A-Z]\\d{8}$";

    // 验证港澳通行证
    public static final String REGEX_ID_TWMT = "^[A-Z]\\d{8}$";

    // 验证护照
    public static final String REGEX_ID_PASSPORT = "^[A-Z](([A-Z])|[0-9])\\d{7}$";


    public static Integer getSexByCardId(String cardId) {
        Integer sex = null;
        Integer sexFlag = Integer.valueOf(cardId.substring(16, 17)) % 2;
        if (sexFlag == 1) {
            sex = 1;
        } else {
            sex = 2;
        }
        return sex;
    }

    public static Date getBirthday(String cardId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String birthdayStr = cardId.substring(6, 14);
        Date birthday = null;
        try {
            birthday = sdf.parse(birthdayStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return birthday;
    }

    /**
     * 验证身份证是否符合国家规范
     * @param
     * @return
     * @author zhy
     * @date 2018/12/3 8:55
     */
    public static boolean cardIdVerify(String cardId) {
        int i = 0;
        String r = "error";
        String lastnumber = "";

        i += Integer.parseInt(cardId.substring(0, 1)) * 7;
        i += Integer.parseInt(cardId.substring(1, 2)) * 9;
        i += Integer.parseInt(cardId.substring(2, 3)) * 10;
        i += Integer.parseInt(cardId.substring(3, 4)) * 5;
        i += Integer.parseInt(cardId.substring(4, 5)) * 8;
        i += Integer.parseInt(cardId.substring(5, 6)) * 4;
        i += Integer.parseInt(cardId.substring(6, 7)) * 2;
        i += Integer.parseInt(cardId.substring(7, 8)) * 1;
        i += Integer.parseInt(cardId.substring(8, 9)) * 6;
        i += Integer.parseInt(cardId.substring(9, 10)) * 3;
        i += Integer.parseInt(cardId.substring(10,11)) * 7;
        i += Integer.parseInt(cardId.substring(11,12)) * 9;
        i += Integer.parseInt(cardId.substring(12,13)) * 10;
        i += Integer.parseInt(cardId.substring(13,14)) * 5;
        i += Integer.parseInt(cardId.substring(14,15)) * 8;
        i += Integer.parseInt(cardId.substring(15,16)) * 4;
        i += Integer.parseInt(cardId.substring(16,17)) * 2;
        i = i % 11;
        lastnumber = cardId.substring(17,18);
        if (i == 0) {
            r = "1";
        }
        if (i == 1) {
            r = "0";
        }
        if (i == 2) {
            r = "x";
        }
        if (i == 3) {
            r = "9";
        }
        if (i == 4) {
            r = "8";
        }
        if (i == 5) {
            r = "7";
        }
        if (i == 6) {
            r = "6";
        }
        if (i == 7) {
            r = "5";
        }
        if (i == 8) {
            r = "4";
        }
        if (i == 9) {
            r = "3";
        }
        if (i == 10) {
            r = "2";
        }
        if (r.equals(lastnumber.toLowerCase())) {
            return true;
        }
        return false;
    }

    public static boolean hkCardVerify(String cardId) {
        return match(REGEX_ID_HKMT, cardId);
    }

    public static boolean passportVerify(String cardId) {
        return match(REGEX_ID_PASSPORT, cardId);
    }

    public static boolean twCardVerify(String cardId) {
        return match(REGEX_ID_TWMT, cardId);
    }

    /**
     * 判断是否符合正则表达式
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 12:51
     */
    private static boolean match(String regex, String str) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}
