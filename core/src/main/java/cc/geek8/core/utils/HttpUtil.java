package cc.geek8.core.utils;


import cc.geek8.core.common.base.CommonConstants;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * HTTP请求工具类
 *
 * @author WanWei
 * @date 2017年6月27日 下午1:14:13
 */
public class HttpUtil {

    /**
     * 发送HTTP POST请求
     *
     * @param url
     * @param body
     * @return
     * @author WanWei
     * @date 2016-4-12 下午4:47:40
     */
    public static  String httpPost(String url, String body) throws Exception {
        String result = "";
        OutputStreamWriter out = null;
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
//			URLConnection conn = realUrl.openConnection();
            HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
//			urlConnection.setConnectTimeout(3000);
//			urlConnection.setRequestMethod("POST");
//			urlConnection.setDoInput(true);// 表示从服务器获取数据
//			urlConnection.setDoOutput(true);// 表示向服务器写数据

            // 设置连接参数
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(20000);

            conn.setRequestProperty("Content-Type","application/json;charset=utf-8");

            // 提交数据
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            out.write(body);
            out.flush();

            // 读取返回数据
            int responseCode = conn.getResponseCode();
            if(responseCode == 200) {
                result = changeInputStream(conn.getInputStream());
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                // do nothing
            }
        }
        return result;
    }

    /**
     * 提取返回数据，转换为字符串
     * @param inputStream
     * @return
     * @author WanWei
     * @date 2018年4月26日 下午2:33:51
     */
    private static String changeInputStream(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        int len = 0;
        String result = "";
        if (inputStream != null) {
            try {
                while ((len = inputStream.read(data)) != -1) {
                    outputStream.write(data, 0, len);
                }
                result = new String(outputStream.toByteArray(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * 获取请求方IP
     *
     * 若是有负载情况下，则需获取x-forwarded-for头里的ip地址
     * 经过负载的请求必须带x-forwarded-for头，记录用户ip
     *
     * @param request
     * @return
     * @author WanWei
     * @date 2016-7-21 上午10:49:18
     */
    public static String getHttpRequestIp(HttpServletRequest request) {

        String ip = request.getHeader("X-Forwarded-For");

        if(StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader(CommonConstants.X_REAL_IP);
        }

        if(StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if(StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if(StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

}
