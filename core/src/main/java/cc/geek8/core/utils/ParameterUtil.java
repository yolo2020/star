package cc.geek8.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 解析特殊符号的工具类
 *
 * @author WanWei
 * @date 2015-8-31 下午2:32:29
 */
public class ParameterUtil {

    // 用${}来表示某些参数
    private static final String PARAM_REG = "\\$\\{([^\\}]+)\\}";

    /**
     * 从${**}中提取参数名称
     *
     * @param original
     * @return
     * @author WanWei
     * @date 2015-8-31 下午2:36:35
     */
    public static List<String> getParameters(String original) {
        List<String> paramters = new ArrayList<String>(0);
        Pattern p = Pattern.compile(PARAM_REG);
        Matcher m = p.matcher(original);
        while (m.find()) {
            paramters.add(m.group(1));
        }

        return paramters;
    }

    /**
     * 模型转换
     * 将字符串列表转换为长整形列表
     * @param strList
     * @return
     * @author WanWei
     * @date 2015-11-9 下午1:37:31
     */
    public static List<Long> convertStringToLongList(List<String> strList){
        List<Long> longList = new ArrayList<Long>(0);
        for(String str : strList){
            longList.add(Long.parseLong(str));
        }

        return longList;
    }

}