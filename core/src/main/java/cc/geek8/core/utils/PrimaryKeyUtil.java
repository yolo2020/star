package cc.geek8.core.utils;


/**
 * @author ZhuangSuyu
 * @date 2018-12-03 21:06
 */
public class PrimaryKeyUtil {
    // fhd_act_traffic 活动交通设置表的前缀
    public static final String ACT_TRAFFIC_PREFIX = "ATraf";

    // fhd_datarange_bizcontrol 权限中数据范围和业务控制
    public static final String DATARANGE_BIZCONTROL_PREFIX = "Rdata";

    // fhd_pay_respinfo 第三方返回的响应消息表前缀
    public static final String PAY_RESPINFO_PREFIX = "Payre";

    // fhd_payment_order 支付订单表主键前缀
    public static final String PAYMENT_ORDER_PREFIX = "";

    // fhd_pay_config 支付配置表主键前缀-微信支付
    public static final String WX_PAY_CONFIG_PREFIX = "PcfWX";

    // fhd_pay_config 支付配置表主键前缀-支付宝
    public static final String ALI_PAY_CONFIG_PREFIX = "PcfAL";

    // fhd_contacter_apply 报名表主键学员报名前缀
    public static final String CONTACTER_APPLY_STU_PREFIX = "CAstu";

    // fhd_contacter_apply 报名表主键义工报名前缀
    public static final String CONTACTER_APPLY_VOL_PREFIX = "CAvol";

    // fhd_order_detail 订单详情（应付账单）ID前缀
    public static final String ORDER_DETAIL_PREFIX = "ODstu";

    // fhd_brand 活动品牌表ID前缀
    public static final String BRAND_PREFIX = "Brand";

    // fhd_act 活动创建表ID前缀
    public static final String ACT_PREFIX = "ACTIV";

    // fhd_act_config 活动设定表ID前缀
    public static final String ACT_CONFIG_PREFIX = "AConf";

    // fhd_act_detail 活动分步介绍ID前缀
    public static final String ACT_DETAIL_PREFIX = "ADtil";

    // fhd_act_check_msg 活动审核模板ID前缀
    public static final String ACT_CHECK_MSG_PREFIX = "ACmsg";

    // fhd_act_question 问卷名称表ID前缀
    public static final String ACT_QUESTION_PREFIX = "AQues";

    // fhd_act_field_option 问卷选项表ID前缀
    public static final String ACT_FIELD_OPTION_PREFIX = "AFOpt";

    // fhd_field_ans 问卷答案记录表ID前缀
    public static final String FIELD_ANS_PREFIX = "AFAns";

    // fhd_team 活动学员班级ID前缀
    public static final String TEAM_STU_PREFIX = "ATstu";

    // fhd_team 活动义工组ID前缀
    public static final String TEAM_VOL_PREFIX = "ATvol";

    // fhd_act_hotelname 活动签到酒店名表ID前缀
    public static final String ACT_HOTELNAME_PREFIX = "Ahtel";

    // fhd_act_hotelroom 活动签到酒店房间表ID前缀
    public static final String ACT_HOTELROOM_PREFIX = "Aroom";

    // fhd_team_reception 签到操作表 学员分班及签到 ID前缀
    public static final String RECEPTION_STU_PREFIX = "Rstud";

    // fhd_team_reception 签到操作表 义工分组及签到 ID前缀
    public static final String RECEPTION_VOL_PREFIX = "Rvolu";

    // fhd_user 用户注册表ID前缀
    public static final String USER_PREFIX = "Urgst";

    // fhd_user_contacter 用户联系人表ID前缀
    public static final String USER_CONTACTER_PREFIX = "UCont";

    // fhd_user_auth 用户认证表ID前缀
    public static final String USER_AUTH_PREFIX = "Uauth";

    // fhd_user_special 特殊用户表ID前缀
    public static final String USER_SPECIAL_PREFIX = "USpcl";

    // fhd_article_content 文章内容表ID前缀
    public static final String ARTICLE_CONTENT_PREFIX = "ACont";

    // fhd_collaboration 签到协同管理表ID前缀
    public static final String STRING_COLLABORATION_PREFIX = "SColl";

    // fhd_collaboration 签到协同管理表ID前缀
    public static final String PAY_ORG_PREFIX  = "APOrg";

    // lms_catalog 文件内容存储目录
    public static final String LMS_CATALOG = "LCata";

    // lms_attachment 文件内容附件
    public static final String LMS_ATTACHMENT = "LAtta";

    // fhd_share 活动分享
    public static final String SHARE_PREFIX = "SHARE";

    // fhd_share_click 点击被分享的活动
    public static final String CLICK_PREFIX = "CLICK";


    /**
     * 生成表主键，统一格式：
     * 表前缀 + yyMMdd + System.currentTimeMillis()后10位 + 4位随机数
     * @param prefix
     * @return String 表主键的前缀
     * @author ZhuangSuyu
     * @date 2018/12/3 21:15
     */
    public static String generatePrimaryKey(String prefix){
        StringBuffer sb = new StringBuffer();
        sb.append(prefix).append("-").append(ToolUtil.getUUID());
        return sb.toString();
    }
}
