package cc.geek8.core.utils;

import org.slf4j.LoggerFactory;

/**
 * 日志工具类
 *
 * @author WanWei
 * @date 2015-10-12 下午7:08:42
 */
public class LogUtil {

    private final static String DB_LOGGER_NAME = "exception_db";

    /**
     * 获得可以将日志写至DB的logger
     * @return
     * @author WanWei
     * @date 2015-10-12 下午7:11:55
     */
    public static ch.qos.logback.classic.Logger getDBLogger(){
        return (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(DB_LOGGER_NAME);
    }

}
