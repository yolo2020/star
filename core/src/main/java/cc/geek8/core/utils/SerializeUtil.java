package cc.geek8.core.utils;

import cc.geek8.core.biz.exception.SystemException;
import de.ruedigermoeller.serialization.FSTObjectInput;
import de.ruedigermoeller.serialization.FSTObjectOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/*
 * 序列化工具类，提供对象与byte[]之间的序列化与反序列化
 *
 * @author WanWei
 * @date 2015-11-6 下午1:10:29
 */
public class SerializeUtil {

    private static final Logger log = LoggerFactory.getLogger(SerializeUtil.class);

    /**
     * 将对象序列化
     *
     * @param obj
     * @param clazz
     * 			class类型
     * @return
     * @author WanWei
     * @date 2015-11-6 下午1:12:45
     */
    public static byte[] objectToByte(Object obj, Class<?> clazz){
        byte[] bytes = null;
        ByteArrayOutputStream baos = null;
        FSTObjectOutput out = null;

        try {
            baos = new ByteArrayOutputStream();
            out = new FSTObjectOutput(baos);
            out.writeObject(obj, clazz);
            out.flush();

            bytes = baos.toByteArray();
        } catch(IOException ioe){
            //抛出业务异常
            new SystemException(SystemException.SERIALIZE_ENCODE_ERROR, ioe, new Object[]{clazz, obj}).throwEx();

        } finally {
            try {
                if(out != null){
                    out.close();
                }
                if(baos != null){
                    baos.close();
                }
            } catch (Exception e) {
                log.warn(e.getMessage(), e);
            }
        }
        return bytes;
    }

    /**
     * 反序列化对象
     *
     * @param bytes
     * @param clazz
     * @return
     * @author WanWei
     * @date 2015-11-6 下午1:21:00
     */
    @SuppressWarnings("unchecked")
    public static <T> T byteToObject(byte[] bytes, Class<T> clazz) {
        T result = null;
        ByteArrayInputStream bi = null;
        FSTObjectInput in = null;
        try {
            bi = new ByteArrayInputStream(bytes);
            in = new FSTObjectInput(bi);
            result = (T)in.readObject(clazz);
        } catch(Exception e){
            //抛出业务异常
            new SystemException(SystemException.SERIALIZE_DECODE_ERROR, e, new Object[]{clazz}).throwEx();
        } finally {
            try {
                if(in != null){
                    in.close();
                }
                if (bi != null) {
                    bi.close();
                }
            } catch (Exception e) {
                log.warn(e.getMessage(), e);
            }
        }

        return result;
    }
}
