package cc.geek8.core.utils;
import java.io.File;
import java.io.StringWriter;
import javax.xml.bind.JAXB;

/**
 * 负责xml与java bean之间的转换的工具类 也可以提供其他xml相关操作
 *
 * @author WanWei
 * @date 2014-6-25 上午10:42:19
 */
public class XmlUtil {

    /**
     * 将XML转换为java bean
     *
     * @param type
     * @param xmlFile
     * @return
     * @author WanWei
     * @date 2014-6-25 上午11:07:16
     */
    public static <T> T xmlToBean(Class<T> type, File xmlFile) {
        T obj = JAXB.unmarshal(xmlFile, type);
        return obj;
    }

    /**
     * 将java bean转换为XML字符串
     *
     * @param bean
     * @return
     * @author WanWei
     * @date 2015-8-26 上午10:52:19
     */
    public static String beanToXml(Object bean) {
        StringWriter writer = new StringWriter();
        JAXB.marshal(bean, writer);
        return writer.toString();
    }

}
