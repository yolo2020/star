package cc.geek8.core.utils;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cc.geek8.core.common.base.CommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.minlog.Log;

/**
 * 无业务内容的一些通用的方法工具类
 *
 * @author WanWei
 * @date 2015-12-17 上午10:21:05
 */
public class ToolUtil {

    private static final Logger log = LoggerFactory.getLogger(ToolUtil.class);

    //英文字母和数字
    private static final Pattern NON_CHINESE_PATTERN = Pattern.compile("[a-zA-Z0-9]+");

    private static final String ONE_BLANK = " ";

    private static final String MORE_BLANK_PATTERN = "[ ]+";

    private static final char UNDERLINE='_';

    /**
     * 从指定类中查找给定方法名的方法
     *
     * @param methodName
     * @param clazz
     * @return
     * @author WanWei
     * @date 2015-12-16 下午2:20:23
     */
    public static Method findMethod(String methodName, Class<?> clazz){
        Method targetMethod = null;
        Method[] methods = clazz.getDeclaredMethods();
        targetMethod = findMethod(methods, methodName);
        if(targetMethod == null){
            log.warn("方法[{}]不在类[{}]声明的方法中，继续从其父类中寻找", methodName, clazz);
            methods = clazz.getMethods();
            targetMethod = findMethod(methods, methodName);
            if(targetMethod == null){
                log.error("在类[{}]及其父类中找不到方法[{}]", clazz, methodName);
            }
        }

        return targetMethod;
    }

    /**
     * 在给定方法数组中查找名称为指定方法名的方法
     *
     * @param methods
     * @param methodName
     * @return
     * @author WanWei
     * @date 2015-12-17 上午9:17:29
     */
    public static Method findMethod(Method[] methods, String methodName){
        for(Method m : methods){
            if(m.getName().equals(methodName)){
                return m;
            }
        }
        Log.warn("没有找到名称为{}的方法，将会返回null值", methodName);
        return null;
    }

    /**
     * 找到字符中的所有字母或数字组成的字符串
     * 在该字符串前填充空格
     * 返回填充后的整个字符串
     *
     * @param originalStr
     * @return
     * @author WanWei
     * @date 2016-9-6 上午11:05:12
     */
    public static String appendBlankNonChineseStr(String originalStr) {
        String result = originalStr;
        Matcher matcher = NON_CHINESE_PATTERN.matcher(originalStr);
        while(matcher.find()) {
            String target = matcher.group();
            result = result.replaceFirst(target, ONE_BLANK.concat(target));
        }
        result = result.trim().replaceAll(MORE_BLANK_PATTERN, ONE_BLANK);
        return result;
    }

    /**
     * 当前部署是否是开发环境
     *
     * @return
     * @author WanWei
     * @date 2016-10-11 上午8:53:00
     */
    public static boolean isCurrentDevEnv() {
        String envType = System.getProperty(CommonConstants.ENV_TYPE);
        if((envType == null) || (envType.equals(CommonConstants.ENV_DEV))) {
            return true;
        }

        return false;
    }

    /**
     * 获取UUID
     *
     * @author WanWei
     * @date 2018-12-10 3:25 PM
     * @param
     * @return java.lang.String
     **/
    public static String getUUID(){
        java.util.UUID uuid = com.fasterxml.uuid.Generators.randomBasedGenerator().generate();
        return uuid.toString();
    }

    /**
     * 驼峰命名转下划线
     * @param param
     * @return String
     * @author ZhuangSuyu
     * @date 2019/3/27 20:42
     */
    public static String camelToUnderline(String param){
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        StringBuilder sb=new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (Character.isUpperCase(c)){
                sb.append(UNDERLINE);
                sb.append(Character.toLowerCase(c));
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
