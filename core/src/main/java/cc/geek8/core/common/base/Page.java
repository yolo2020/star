package cc.geek8.core.common.base;

import java.io.Serializable;
import java.util.List;

/**
 * 封装分页查询结果
 *
 * @author WanWei
 * @date 2015-10-19 下午3:12:12
 */
public class Page<T> implements Serializable{

    private static final long serialVersionUID = 70394466218393861L;
    // 记录总数
    private long totalCount;
    // 每页显示记录条数
    private int pageSize;
    // 展示的页数
    private int pageNum;
    //总页数
    private int pages;

    // 返回的查询结果集
    private List<T> result;

    public Page() {
    }

    /**
     * 从分页插件中获取分页查询后的数据
     * @param result
     */
    public Page(List<T> result) {
        com.github.pagehelper.Page<T> page = (com.github.pagehelper.Page<T>) result;
        this.totalCount = page.getTotal();
        this.pageSize = page.getPageSize();
        this.pages = page.getPages();
        this.pageNum = page.getPageNum();
        this.result = page.getResult();
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

}
