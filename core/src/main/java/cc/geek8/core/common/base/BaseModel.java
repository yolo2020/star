package cc.geek8.core.common.base;


import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 所有数据模型的基类
 * 提供一些公共的设置
 *
 * @author WanWei
 * @date 2015-10-14 下午2:23:23
 */
public class BaseModel implements Serializable {

    private static final long serialVersionUID = 5210759488664813185L;

    private static Logger log = LoggerFactory.getLogger(BaseModel.class);

//	private static final String PASS_FIELD_NAME = "serialVersionUID";


    //Fullwisdom数据权限专用，多个角色ID之间用逗号分隔
    protected String roles;

    //以map的形式保存一些扩充的信息，如Dictionary等
    protected Map<Object, Object> attached = new HashMap<Object, Object>(0);


    public Map<Object, Object> getAttached() {
        return attached;
    }

    /**
     * 添加扩充信息
     *
     * @param key
     * @param value
     * @author WanWei
     * @date 2015-11-3 下午1:48:13
     */
    public void addAttached(String key, Object value) {
        attached.put(key, value);
    }

    /**
     * 清空扩充信息
     *
     * @author WanWei
     * @date 2015-11-3 下午1:49:48
     */
    public void cleanAttached(){
        attached.clear();
    }

    /**
     * 根据key值提取扩充信息
     *
     * @param key
     * @return
     * @author WanWei
     * @date 2015-11-3 下午1:46:51
     */
    public Object getAttached(Object key) {
        return attached.get(key);
    }


    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

}

