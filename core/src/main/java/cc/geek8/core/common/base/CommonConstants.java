package cc.geek8.core.common.base;

public interface CommonConstants  {

    //方言
    String DB_DIALECT = "dialect";

    //关系数据库类型 - PostgreSQL
    String DB_NAME_POSTGRESQL = "postgresql";

    // 系统环境变量 指明服务类型
    String SERVICE_TAG = "service_tag";

    // 服务类型：后端 - server
    String SERVICE_VALUE = "server";

    // 用来表示生产或开发环境
    String ENV_TYPE = "ENV_TYPE";

    //系统变量，设置加载哪些配置文件
    String CONFIG_FILE_FILTER = "plat_config_filter";

    //冒号字符串表示
    String STR_COLON = ":";

    /*
     * 模块名称标识
     *
     */
    String MODULE_NAME = "MODULE_NAME";


    // 开发环境
    String ENV_DEV = "dev";

    // 生产环境
    String ENV_PRODUCT = "production";

    String USER_ACCOUNT = "user_account";// 用户账号

    String USER_ID = "user_id"; // userId

    String USER_PWD = "user_password";// 用户密码

    String USER_NAME = "user_name";// 用户真实姓名

    String SESSION_CODE = "user_phone";// 用户手机号

    String USER_IP = "user_ip";// 用户发送请求的IP

    String USER_BIZ_PERMISSION = "user_biz_permission";//用户的业务权限

    // IP标识
    String X_REAL_IP = "X-Real-IP";

    // 全文搜索支撑类型：db - postgresql数据库
    String TEXT_SEARCH_TYPE_DB = "db";

    // 全文搜索支撑类型：lucene - lucene搜索引擎
    String TEXT_SEARCH_TYPE_LUCENE = "lucene";

    // 暂存用户在线状态的缓存名称
    String USER_ONLINE_CACHE = "user_online_cache";

    // 暂存用户session
    String USER_SESSION_CACHE = "user_session_cache";

    //时间限制权限控制缓存名称
    String TIME_LIMIT_PERMISSION_CACHE = "time_limit_permission_cache";

    String PASS_FIELD_NAME = "serialVersionUID";

    //系统使用的编码
    String CHARSET_ENCODING = "UTF-8";

}
