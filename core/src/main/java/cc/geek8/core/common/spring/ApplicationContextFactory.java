package cc.geek8.core.common.spring;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 为单元测试准备的Spring上下文
 *
 * @author WanWei
 * @date 2015-9-10 下午4:24:33
 */
@Component
public class ApplicationContextFactory implements ApplicationContextAware {

    private static ApplicationContext context = null;

    /**
     * 通过文件系统加载器加载Spring配置文件
     *
     * @param files
     * @author WanWei
     * @date 2014-6-24 下午5:06:15
     */
    public static void initialize(String[] files) {
        if (files != null) {
            ApplicationContext contextVar = new ClassPathXmlApplicationContext(files);
            if (context == null) {
                context = contextVar;
            }
        }
    }

    /**
     * 设置spring上下文
     *
     * @param context
     * @author WanWei
     * @date 2015-9-9 上午9:06:45
     */
    public static void setContext(ApplicationContext context) {
        ApplicationContextFactory.context = context;
    }

    /**
     * 根据指定的注入类的类型获取该类被Spring管理的实例
     *
     * @param type
     * @return
     */
    public static <T> T getBean(Class<T> type) {
        if (context == null) {
            return null;
        }
        return context.getBean(type);
    }

    /**
     * 根据指定的注入类的Id，获取该类被Spring管理的实例
     *
     * @param beanId
     * @param requiredType
     * @return
     */
    public static <T> T getBean(String beanId, Class<T> requiredType) {
        if (context == null) {
            return null;
        }
        return context.getBean(beanId, requiredType);
    }

    /**
     * 根据指定的注入类的Id，获取该类被Spring管理的实例，未添加泛型
     *
     * @param beanId
     * @return
     */
    public static Object getBean(String beanId) {
        if (context == null) {
            return null;
        }
        return context.getBean(beanId);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (context == null) {
            context = applicationContext;
        }
    }

    /**
     * 关闭Spring服务，即关闭Spring Boot服务
     *
     * @author WanWei
     * @date 2018-12-12 2:33 PM
     * @param
     * @return void
     **/
    public static void close() {
        if(context == null) {
            return;
        }

        if (context instanceof ConfigurableApplicationContext) {
            ((ConfigurableApplicationContext) context).close();
        } else if (context instanceof ClassPathXmlApplicationContext) {
            ((ClassPathXmlApplicationContext) context).close();
        }

    }

}
