package cc.geek8.core.common.base;


 /* web用到的一些静态常量
         *
         * @author WanWei
        * @date 2016-4-29 上午8:57:31
        */

public interface WebConstants {

    //user session key
    String UESER_SESSION_INFO_KEY = "user_session_info";

    //weixin session key
    String SESSION_WX_INFO_KEY = "session_wx_info";

    // 暂存用户在线状态的缓存名称
    public static final String USER_ONLINE_CACHE = "user_online_cache";

    // 暂存用户session
    public static final String USER_SESSION_CACHE = "user_session_cache";

    // 暂存URL请求的访问记录
    public static final String URL_ACCESS_FREQUENCY_CACHE = "url_access_frequency_cache";

    // HTTPS认证文件路径
    public static final String AUTH_HTTPS_PATH = "/auth/plat_keystore.jks";

    // HTTPS认证密码
    public static final String AUTH_HTTPS_PASSWORD = "pushi123";

    // JWT用到的sign前缀
    public static final String TOKEN_STRING = "jwt_token";

    // 用来做AES加密解密的IV设置，固定设置为32个1
    public static final String IV_AES = "11111111111111111111111111111111";

    // 用来封装加密参数的可以值
    public static final String ENCRYPT_PARAMS_KEY = "pushi_params";

    // login解密
    public static final String IV_KEY = "21a361d96e3e13f5";

    // 识别业务异常的异常码
    public static final String ECODE = "ecode";

    // web端Spring扫描的包路径
    public static final String WEB_BASE_SCAN_PACKAGE = "com.aplat.web";

    //导出表格时获取到的属性值
    public static final String EXPORT_EXCEL_NULL="------";
}
