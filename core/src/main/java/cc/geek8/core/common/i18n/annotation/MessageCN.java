package cc.geek8.core.common.i18n.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 对应中文国际化的注解
 *
 * @author WanWei
 * @date 2015-9-8 上午9:32:45
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MessageCN {
    String value() default "";

    String explanation() default "";
}
