package cc.geek8.core.common.base;

import java.net.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;


public class ExternalitiesVerify  {

    private static Logger log = LoggerFactory.getLogger(ExternalitiesVerify.class);

    //DB地址的前缀
    private static final String DATABASE_URL_SP = "jdbc:postgresql://";

    //DB的默认端口
    private static final String DATABASE_DEFAULT_PORT = "5432";

    //是否跳过校验
    @Value("${skip.externalities.verify}")
    private boolean skipVerify = false;

    //ZooKeeper的IP及端口
    @Value("${zk.connect}")
    private String zookeeperAddr;

    //数据库的URL地址
    @Value("${datasource.jdbc.url}")
    private String databaseUrl;

    /**
     * 检查外部环境是否存在
     * 若不存在则退出系统
     *
     * @author WanWei
     * @date 2015-10-30 下午6:59:13
     */
    public void verify(){
        if(skipVerify){
            log.warn("根据配置跳过了外部环境的检查，这可能会导致系统无法正常运行");
            return;
        }
        log.info("开始检查ZooKeeper服务是否可用...");
        if(!checkZooKeeper()){
            System.exit(1);
        }
        log.info("ZooKeeper服务[{}]可用", zookeeperAddr);
        log.info("开始检查数据库服务是否可用...");
        if(!checkDateBase()){
            System.exit(1);
        }
        log.info("数据库服务[{}]可用", databaseUrl);

    }

    /**
     * 检查ZooKeeper服务是否可用
     *
     * @return
     * @author WanWei
     * @date 2015-10-30 下午7:18:06
     */
    private boolean checkZooKeeper(){
        return checkAddress(zookeeperAddr, "ZooKeeper");
    }

    /**
     * 检查数据库服务是否可用
     * @return
     * @author WanWei
     * @date 2015-10-30 下午7:34:31
     */
    private boolean checkDateBase(){
        if((databaseUrl == null) || databaseUrl.isEmpty()){
            log.error("系统未配置数据库的地址，系统无法正常启动，请检查配置文件[application-*.properties]");
            return false;
        }

        if(!databaseUrl.startsWith(DATABASE_URL_SP)){
            log.error("系统不支持的数据库类型或数据库URL[{}]配置错误，系统无法正常启动，系统仅支持Postgresql数据库", databaseUrl);
            return false;
        }

        String address = databaseUrl.replace(DATABASE_URL_SP, "");
        address = address.substring(0, address.indexOf("/"));
        if(address.indexOf(":") == -1){
            address = address.concat(":").concat(DATABASE_DEFAULT_PORT);
        }

        return checkAddress(address, "DB");
    }

    /**
     * 针对IP:Port的格式进行检查
     * 并检查该IP下的服务端口是否开启
     * @param address
     * @param name
     * 		 地址类型  如:数据库或ZooKeeper
     * @return
     * @author WanWei
     * @date 2015-10-30 下午7:24:47
     */
    private boolean checkAddress(String address, String name){
        if((address == null) || address.isEmpty()){
            log.error("系统未配置{}的地址，系统无法正常启动，请检查配置文件[application-*.properties]", name);
            return false;
        }
        String[] addrInfo = address.split(":");
        if(addrInfo.length != 2){
            log.error("{}地址格式不正确, 请参照正确格式：IP:Port", name);
            return false;
        }
        int port = -1;
        String ip = addrInfo[0];
        String portStr = addrInfo[1];
        try{
            port = Integer.parseInt(portStr);
        }catch(Exception e){
            log.error("{}地址端口不正确， 即Port[{}]， 请确保Port为小于65535的整型数字", name, portStr);
            return false;
        }
        if(!checkAddrInActive(ip, port)){
            log.error("访问{}地址失败， 请确定ZooKeeper[{}]服务是否启用", name, address);
            return false;
        }

        return true;
    }

    /**
     * 检测指定IP的端口是否开启
     *
     * @param ip
     * @param port
     * @return
     * @author WanWei
     * @date 2015-10-30 下午7:14:40
     */
    private boolean checkAddrInActive(String ip, int port){
        try{
            Socket s = new Socket(ip, port);
            s.close();
        }catch(Exception e){
            return false;
        }
        return true;
    }

    public String getZookeeperAddr() {
        return zookeeperAddr;
    }

    public void setZookeeperAddr(String zookeeperAddr) {
        this.zookeeperAddr = zookeeperAddr;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public void setDatabaseUrl(String databaseUrl) {
        this.databaseUrl = databaseUrl;
    }

    public boolean isSkipVerify() {
        return skipVerify;
    }

    public void setSkipVerify(boolean skipVerify) {
        this.skipVerify = skipVerify;
    }
}

