package cc.geek8.core.biz.exception;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.geek8.core.biz.exception.annotation.BizException;
import cc.geek8.core.common.spring.ApplicationContextFactory;
import cc.geek8.core.utils.LogUtil;


/**
 * 业务异常的基类
 *
 * @author WanWei
 * @date 2015-9-8 上午9:41:44
 */
@BizException
public abstract class BaseBizException extends RuntimeException {

    private static final long serialVersionUID = -8059002982865298584L;

    private static Logger log = LoggerFactory.getLogger(BaseBizException.class);

    // 异常码数字部分的长度,固定为6
    private static final int CODE_NUMBER_LENGTH = 6;

    private static final String DEFAULT_ID = "SERVER";

    // 用来格式化数字，左侧补零
    private static NumberFormat nf = NumberFormat.getInstance();

    // 异常码
    private String eCode;

    // java原生异常的堆栈信息
    private String stackMsg;

    // java原生异常
    private Exception original;

    // 异常信息需要传递的参数
    private Object[] params;

    // 异常说明信息
    private String errorMsg;

    // 一些重要的断言信息
    private Map<String, Object> keyPoints = new HashMap<String,Object>(0);

    // 国际化信息
    private Locale locale = Locale.CHINA;

    @Resource
  //  private ExceptionI18nMsgHelper helper;

    //简要异常信息
    private ExceptionSimpleInfo sInfo = new ExceptionSimpleInfo();

    /**
     * 初始数字格式化工具 5位数字，左侧补零
     *
     * @author WanWei
     * @date 2015-09-08 14:42:22
     */
    static {
        nf.setGroupingUsed(false);
        nf.setMaximumIntegerDigits(CODE_NUMBER_LENGTH);
        nf.setMinimumIntegerDigits(CODE_NUMBER_LENGTH);
    }

    /**
     * 空的构造函数是必须的
     */
    protected BaseBizException(){

    }

    /**
     * 构造方法，code不允许为空，其余参数可能设定null
     *
     * @param code
     * @param original
     * @param params
     *
     * @author WanWei
     * @date 2015-09-08 15:27:55
     */
    protected BaseBizException(@NotNull String code, Exception original, Object... params) {
        this.eCode = code;
        this.original = original;
        this.params = params;
        this.stackMsg = Trace.getStackTrace(this.original);
        if(original != null){
            //打印原生异常
            log.error(original.getMessage(), original);
        }
        sInfo.setEcode(this.eCode);
        logToDB(this);
    }

    /**
     * 业务异常的描述信息也是已数据字典维护的
     * 在spring加载的时候提取数据字典数据
     *
     * @author WanWei
     * @date 2015-10-19 上午9:03:58
     */
    /*
    @PostConstruct
    public void initDictionary(){
        Class<? extends BaseBizException> currentClass = getClass();
        helper.cacheAndRecordI18nMessage(currentClass);
    }*/

    /**
     * 抛出异常，保存日志到DB等
     *
     * @author WanWei
     * @date 2014-6-23 下午5:42:39
     */
    public final <T extends BaseBizException> void throwEx()
            throws BaseBizException {
        throw this;
    }


    /**
     * 获取带国际化描述信息的异常信息
     *
     * @author WanWei
     * @date 2015-09-08 14:42:12
     */
    /*
    public String getErrorMsg() {

        if ((this.errorMsg == null) || this.errorMsg.isEmpty()) {
            //直接new出来的异常类helper为null
            if(helper == null){
                try{
                    helper = ApplicationContextFactory.getBean(ExceptionI18nMsgHelper.class);
                }catch(Exception e){
                    //后端传出去的exception其errorMsg必然不为null或空
                    log.error(e.getMessage(), e);
                    return this.errorMsg;
                }
            }
            if (params == null) {
                this.errorMsg = helper.getBizMessage(eCode, locale);
            } else {
                this.errorMsg = helper.getBizMessage(eCode, params, locale);
            }
        }
        sInfo.setErrorMsg(this.errorMsg);
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        sInfo.setErrorMsg(this.errorMsg);
    }

    @Override
    public String getMessage() {
        // TODO Auto-generated method stub
        return getErrorMsg();
    }*/

    public String getOriginMessage() {
        return stackMsg;
    }

    public String getEcode() {
        return this.eCode;
    }

    public Object[] getParams() {
        return params;
    }

    public Exception getOriginal() {
        return original;
    }

    public Map<String, Object> getKeyPoints() {
        return keyPoints;
    }

    public void setKeyPoints(Map<String, Object> keyPoints) {
        this.keyPoints = keyPoints;
    }

    /**
     * 添加断言辅助信息
     *
     * @param key
     * @param value
     * @author WanWei
     * @date 2015-10-13 下午5:13:31
     */
    public void addKeyPoint(String key, Object value){
        keyPoints.put(key, value);
    }

    /**
     * 提供此方法是为了规范异常码的写法,但是这么做会影响程序启动时的效率 启动时间多个几秒影响不是很大，可以忍
     *
     * @param currentSystemId
     *            子系统代号 如：ID_PLATFORM
     * @param code
     * @return String
     * @author WanWei
     * @date 2014-6-26 下午7:59:10
     */
    public static String getFormatedNumber(String currentSystemId, int code) {
        if ((currentSystemId != null) && (!currentSystemId.isEmpty())) {
            return currentSystemId.concat(nf.format(code));
        }
        return nf.format(code);
    }

    /**
     * 记录日志到数据库
     *
     * @param ee
     * @author WanWei
     * @date 2015-10-12 下午7:03:08
     */
    protected <T extends BaseBizException> void logToDB(T ee) {
        if (ee == null) {
            log.error("异常实体为null");
            return;
        }

     //   log.error("业务异常 - 异常码[{}]  描述：{}", ee.getEcode(), ee.getErrorMsg());

        if(this.original != null){
            //打印原始异常堆栈 Add by WanWei 2015-09-08 16:57:23
            log.error("原始异常堆栈", this.original);
        }

        //将日志写到数据库 TODO 一些信息还没有存进去 后续追加
     //   LogUtil.getDBLogger().error(null, ee.getErrorMsg(), ee.getEcode(), new Exception(ee.getErrorMsg(), ee.getOriginal()));
    }

    /**
     *
     * @param moduleId
     * @param code
     * @return
     * @author WanWei
     * @date 2016-4-13 下午1:13:40
     */
    protected static String createExceptionCode(String moduleId, int code) {
        if(moduleId == null){
            moduleId = DEFAULT_ID;
        }
        return getFormatedNumber(moduleId, code);
    }

    public ExceptionSimpleInfo getExceptionSimpleInfo() {
        return sInfo;
    }
}
