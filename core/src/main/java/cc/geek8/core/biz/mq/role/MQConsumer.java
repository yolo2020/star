package cc.geek8.core.biz.mq.role;


import io.jafka.consumer.Consumer;
import io.jafka.consumer.ConsumerConfig;
import io.jafka.consumer.ConsumerConnector;
import io.jafka.consumer.MessageStream;
import io.jafka.utils.ImmutableMap;
import cc.geek8.core.biz.cache.api.DCacheService;
import cc.geek8.core.biz.mq.EventPublisher;
import cc.geek8.core.biz.mq.serializer.MQMessageDecoder;
import cc.geek8.core.biz.mq.service.AplatMQConfig;
import cc.geek8.core.biz.mq.service.IMQMessageListener;
import cc.geek8.core.biz.mq.service.MQEventAdapter;
import cc.geek8.core.biz.mq.service.MQMessage;
import cc.geek8.core.common.spring.ApplicationContextFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 消息消费者
 *
 * @author WanWei
 * @date 2015-10-27 下午4:53:56
 */
public class MQConsumer {

    private static Logger log = LoggerFactory.getLogger(MQConsumer.class);

    // 分类保存消费消息监听
    private Map<String, List<IMQMessageListener>> listeners = new HashMap<String, List<IMQMessageListener>>(0);

    // 默认标签
    private static final String DEFAULT_TAG = "null";

    //一个消费者仅开启一个线程处理，如此是为了保证手动commit消息的准确性
    private static final int FIX_MESSAGE_HANDLE_THREAD = 1;

    // 主题
    private String topic;

    // 表示该消费者的ID
    private String consumerId;

    // 表示该消费者所在的组
    private String groupId;

    // Jafka ConsumerConnector
    private ConsumerConnector connector;

    // 是否开始监听
    private boolean receiving = false;

    private boolean manualCommit = false;

    /**
     * 根据指定的topic和 Zookeeper连接创建消费者
     *
     * @param topic
     * @param zkConnect
     * @param groupId
     *            针对同一个topic，一个消息只会被属于同一个groupId的一个消费者消费， 使用该限制可以实现queue的应用方式
     * @param manualCommit
     * 			  手动提交
     */
    public MQConsumer(String topic, String zkConnect, String groupId, boolean manualCommit) {
        // zkConnect若为null，则采用配置文件中设定的值
        if (zkConnect == null) {
            zkConnect = AplatMQConfig.getConfigValue(AplatMQConfig.ZOOKEEPER_CONNECT);
        }
        Properties props = new Properties();
        props.put(AplatMQConfig.ZOOKEEPER_CONNECT, zkConnect);
        if ((groupId == null) || groupId.isEmpty()) {
            groupId = AplatMQConfig.DEFAULT_CONSUMER_GROUP;
        }
        this.groupId = groupId;
        props.put(AplatMQConfig.CONSUMER_GROUPID, groupId);
        props.put(AplatMQConfig.ZK_CONNECTION_TIMEOUT_MS, AplatMQConfig.getConfigValue(AplatMQConfig.ZK_CONNECTION_TIMEOUT_MS));

        // 其他配置, 不需要经常更改的配置暂时硬编码 TODO
        props.put("fetcher.backoff.ms", "500");
        // 将自动提交关掉，待跟消息相关的业务处理结束后再手动提交关闭消息
        props.put("autocommit.enable", "false");
        props.put("zookeeper.sync.time.ms", "6000");


        // 采用UUID随机生成消费者ID
        this.consumerId = UUID.randomUUID().toString();
        props.put(AplatMQConfig.CONSUMER_ID, consumerId);

        ConsumerConfig consumerConfig = new ConsumerConfig(props);
        connector = Consumer.create(consumerConfig);
        if(topic == null || topic.isEmpty()){
            topic = AplatMQConfig.getSystemTopic();
        }
        this.topic = topic;
        this.manualCommit = manualCommit;
    }

    /**
     * 开始监听消息并处理
     *
     * @author WanWei
     * @date 2015-10-28 上午9:17:53
     */
    public void startReceiving() {
        if (listeners.isEmpty()) {
            log.warn("MQConsumer[{}]没有消息处理类，无法接收APlat-MQ[{}]产生的消息",consumerId, topic);
        }

        if(!receiving){
            Map<String, List<MessageStream<MQMessage>>> topicMessageStreams = connector.createMessageStreams(
                    ImmutableMap.of(topic, FIX_MESSAGE_HANDLE_THREAD),// 仅内部采用1个线程处理处理stream
                    new MQMessageDecoder());
            List<MessageStream<MQMessage>> streams = topicMessageStreams.get(topic);
            ExecutorService executor = Executors.newFixedThreadPool(FIX_MESSAGE_HANDLE_THREAD);

//			// 获取访问缓存的对象
//			final CacheClient cacheClient = ApplicationContextFactory.getBean(CacheClient.class);

            for (final MessageStream<MQMessage> stream : streams) {
                executor.submit(new Runnable() {

                    public void run() {
                        try{
                            DCacheService cacheClient = ApplicationContextFactory.getBean(DCacheService.class);;
                            // 需要借助缓存达到错误重发的目的
                            while(cacheClient == null) {
                                cacheClient = ApplicationContextFactory.getBean(DCacheService.class);
                                //启动期可能会出现这种情况,等待CacheClient完成注入
                                Thread.sleep(1000);
                            }

                            // 获取访问缓存的对象
                            for (MQMessage message : stream) {
                                if(message.getData() == null) {
                                    log.error("反序列化失败的消息，系统不会处理，消息ID:[{}]", message.getId());
                                    //关闭消息后继续
                                    connector.commitOffsets();
                                    continue;
                                }
                                String tag = message.getTag();
                                if (message.getTag() == null) {
                                    tag = DEFAULT_TAG;
                                }

                                List<IMQMessageListener> listenerList = listeners.get(tag);

                                if(listenerList == null) {
                                    // 没有可以处理消息的listener，关闭消息后继续
                                    connector.commitOffsets();
                                    continue;
                                }

                                // 收集执行失败的记录
                                Set<String> failSet = new HashSet<String>();
                                String messageId = message.getId();
                                //使用messageId + group的字符串hash值做缓存key
                                String key = String.valueOf(messageId.concat(groupId).hashCode());

                                String cacheName = null;


                                // 如果其中存在处理比较慢的listener则会影响整体效率 TODO 以后处理
                                // record by WanWei 2015-10-28
                                for (IMQMessageListener listener : listenerList) {
                                    String listenerId = null;
                                    if (listener instanceof MQEventAdapter) {
                                        listenerId = ((MQEventAdapter) listener).getListenerId();
                                        cacheName = listenerId;
                                    }

                                    // 记录已经成功处理了该message的listener
                                    List<Object> cacheData = null;

                                    if(cacheName != null) {
                                        cacheData = cacheClient.getKeys(cacheName);
                                    }

                                    if (cacheData != null) {
                                        if (cacheData.contains(key)) {
                                            // 已经执行成功的跳过
                                            continue;
                                        }
                                    }

                                    try {
                                        listener.onMessage(message);
                                    } catch (Exception e) {
                                        if (listenerId != null) {
                                            log.error("处理消息出现错误，该Consumer将开启重试机制", e);
                                            failSet.add(key);
                                        } else {
                                            log.error("处理消息出现错误，根据消息设置系统将会忽略该消息", e);
                                        }
                                    }
                                }

                                if(!manualCommit) {
                                    // 将正常消费的消息提交
                                    connector.commitOffsets();
                                }

                                if (failSet.isEmpty()) {
                                    if(cacheName != null) {
                                        // 清除缓存
                                        cacheClient.remove(cacheName, key);
                                    }

                                } else {
                                    //2000毫秒后重发消息
                                    Thread.sleep(2000);
                                    EventPublisher.getInstance().sendMessage(topic, message);
                                }

                                log.debug("从主题[{}]收到消息, Tag:{}", topic, tag);
                            }
                        }catch(Exception e){
                            //放弃对某类消息的监听
                            log.error("数据序列化出现问题，系统无法监听该类消息，此消费者无效", e);
                        }
                    }
                });
            }
            receiving = true;
        }
    }

//		if (listenThread == null) {
//			holdSign = new CountDownLatch(1);
//			listenThread = new Thread() {
//
//				@Override
//				public void run() {
//					while(true){
//						if(ApplicationContextFactory.getBean(CacheClient.class) != null){
//							break;
//						}
//					}
//					Map<String, List<MessageStream<MQMessage>>> topicMessageStreams = connector.createMessageStreams(
//							ImmutableMap.of(topic, MESSAGE_HANDLE_THREAD_COUNT),// 默认内部采用2个线程处理处理stream
//							new MQMessageDecoder());
//					List<MessageStream<MQMessage>> streams = topicMessageStreams.get(topic);
//					ExecutorService executor = Executors.newFixedThreadPool(MESSAGE_HANDLE_THREAD_COUNT);
//					// 获取访问缓存的对象
//					final CacheClient cacheClient = ApplicationContextFactory.getBean(CacheClient.class);
//					for (final MessageStream<MQMessage> stream : streams) {
//						executor.submit(new Runnable() {
//
//							public void run() {
//								for (MQMessage message : stream) {
//									String tag = message.getTag();
//									if (message.getTag() == null) {
//										tag = DEFAULT_TAG;
//									}
//									// 收集执行失败的记录
//									Set<String> failSet = new HashSet<String>();
//									String messageId = message.getId();
//									//使用messageId + group的字符串hash值做缓存名
//									String cacheName = String.valueOf(messageId.concat(groupId).hashCode());
//
//									// 记录已经成功处理了该message的listener
//									System.out.println(cacheClient);
//									List<Object> cacheData = cacheClient.getKeys(cacheName);
//
//									List<IMQMessageListener> listenerList = listeners.get(tag);
//
//									// 如果其中存在处理比较慢的listener则会影响整体效率 TODO 以后处理
//									// record by WanWei 2015-10-28
//									for (IMQMessageListener listener : listenerList) {
//										String listenerId = null;
//										if (listener instanceof MQEventAdapter) {
//											listenerId = ((MQEventAdapter) listener).getListenerId();
//										}
//										if (cacheData != null) {
//											if (cacheData.contains(listenerId)) {
//												// 已经执行成功的跳过
//												continue;
//											}
//										}
//
//										try {
//											listener.onMessage(message);
//										} catch (Exception e) {
//											if (listenerId != null) {
//												log.error("处理消息出现错误，该Consumer将开启重试机制", e);
//												failSet.add(listenerId);
//											} else {
//												log.error("处理消息出现错误，根据消息设置系统将会忽略该错误", e);
//											}
//										}
//									}
//									if (failSet.isEmpty()) {
//
//										// 将正常消费的消息提交
//										connector.commitOffsets();
//										// 清除缓存
//										cacheClient.remove(cacheName);
//
//									} else {
//										//如果有问题则开启重试机制
//										stopReceiving();
//									}
//
//									log.debug("从主题[{}]收到消息, Tag:{}", topic, tag);
//								}
//							}
//						});
//					}
//					listenThread.setName("MQConsumer[" + consumerId + "]");
//					listenThread.start();
//					try {
//						holdSign.await();
//					} catch (Exception e) {
//						log.error("Hold消费监听线程[{}]失败, 消费者[{}]失效", currentThread().getName(), consumerId);
//					}
//					executor.shutdownNow();
//					// 重启监听
//					log.info("消费者[{}]监听任务重启", consumerId);
//					startReceiving();
//				}
//			};

//		}

//	}

    /**
     * 公开出来便于手动控制消息提交事务
     *
     * @author WanWei
     * @date 2016-5-16 上午11:10:31
     */
    public void commitOffsets() {
        connector.commitOffsets();
    }

    /**
     * 添加消息监听
     *
     * @param tag
     *            标签，用来过滤信息
     * @param listener
     * @author WanWei
     * @date 2015-10-28 上午8:50:10
     */
    public void addMessageListener(String tag, IMQMessageListener listener) {
        if ((tag == null) || tag.isEmpty()) {
            tag = DEFAULT_TAG;
        }
        List<IMQMessageListener> listenerList = listeners.get(tag);
        if (listenerList == null) {
            listenerList = new ArrayList<IMQMessageListener>(0);
            listeners.put(tag, listenerList);
        }
        listenerList.add(listener);
    }
}
