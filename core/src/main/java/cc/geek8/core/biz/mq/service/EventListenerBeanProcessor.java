package cc.geek8.core.biz.mq.service;


import cc.geek8.core.biz.cache.api.DCacheService;
import cc.geek8.core.biz.exception.BaseBizException;
import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.biz.mq.EventHandler;
import cc.geek8.core.biz.mq.annotation.EventListener;
import cc.geek8.core.biz.mq.annotation.OnEvent;
import cc.geek8.core.biz.validation.annotation.Validation;
import cc.geek8.core.common.spring.ApplicationContextFactory;
import cc.geek8.core.utils.ToolUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * 扫描标注了@EventListener的类并提取及标注了@OnEvent的方法
 *
 * @author WanWei
 * @date 2015-10-28 下午3:10:14
 */
public class EventListenerBeanProcessor implements BeanPostProcessor,
        PriorityOrdered {

    private static Logger log = LoggerFactory
            .getLogger(EventListenerBeanProcessor.class);

    // 在这里完成资源注入
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        Class<?> clazz = bean.getClass();
        if (clazz.isAnnotationPresent(EventListener.class)) {
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(OnEvent.class)) {

                    OnEvent event = method.getAnnotation(OnEvent.class);
                    String group = event.group();
                    // 加入到消息监听
                    String listenerId = clazz.getName().concat(".").concat(method.getName());
                    if((group == null) || group.isEmpty()){
                        // 使用方法的完整路径做groupId
                        group= listenerId;
                    }


                    final String topic = event.topic();
                    final String tag = event.tag();
                    int poolSize = event.poolSize();
                    final boolean ignoreException = event.ignoreException();
                    final boolean fuzzyMatch = event.fuzzyMatch();
                    final Method onEventMethod = method;
                    final Object onEventClassInstance = bean;
                    final String onEventGroup = group;
                    MQEventAdapter listener = new MQEventAdapter() {

                        public void onMessage(MQMessage message) {
                            DCacheService cacheClient = ApplicationContextFactory.getBean(DCacheService.class);
                            Object unknown = message.getData();
                            if (!(unknown instanceof Object[])) {
                                log.debug("消息实体的数据类型不是Object[], 无需处理");
                                return;
                            }
                            Object[] params = (Object[]) message.getData();
                            Class<?>[] paramterClasses = onEventMethod.getParameterTypes();
                            //传递给方法的实际参数
                            Object[] realParams = new Object[paramterClasses.length];

                            //检查是否接收原生的MQMessage
                            if((paramterClasses.length == 1) && MQMessage.class.equals(paramterClasses[0])) {
                                //参数是MQMessage类型时直接将message传给处理方法
                                realParams[0] = message;
                            }else{
                                // 检查方法入参是否匹配
                                //精确匹配
                                if (!fuzzyMatch && !checkParamsTypeMatch(paramterClasses, params, realParams)) {
                                    return;
                                }

                                //模糊匹配
                                if (fuzzyMatch && !checkParamsTypeMatchFuzzy(paramterClasses, params, realParams)) {
                                    return;
                                }
                            }

                            try {
                                processValidate(onEventMethod, realParams);
                                onEventMethod.invoke(onEventClassInstance, realParams);
                                //使用messageId + group的字符串hash值做缓存key
                                String cacheName = this.getListenerId();
                                String key = String.valueOf(message.getId().concat(onEventGroup).hashCode());
                                //执行成功则记录到缓存中
                                cacheClient.put(cacheName, key, null);
                            } catch(IllegalArgumentException iea){
                                //对集合、Map内部类型做检查的成本不如直接调用让其抛异常
                                //参数不匹配的异常无需处理
                                log.debug("参数类型不匹配，对于集合或Map内部的类型未做参数类型匹配检验", iea);
                            } catch(InvocationTargetException el) {
                                Throwable realException = el.getTargetException();
                                Class<?> clazz = onEventClassInstance.getClass();
                                String methodName = onEventMethod.getName();
                                log.error("处理消息失败, 发生InvocationTargetException错误", el);
                                //如果设置为忽略处理异常，则不发送系统异常，即不会引发重试动作
                                if(!ignoreException){
                                    if(realException instanceof BaseBizException){
                                        throw (BaseBizException)realException;
                                    }
                                    new SystemException(SystemException.MQ_MESSAGE_HANDLE_ERROR, el, new Object[]{clazz, methodName}).throwEx();
                                }

                            } catch (Exception e) {
                                Class<?> clazz = onEventClassInstance.getClass();
                                String methodName = onEventMethod.getName();
                                log.error("处理消息失败, 对应的处理方法:{}-{} Topic[{}] Tag[{}]", clazz, methodName, topic, tag);
                                log.error(e.getMessage(), e);
                                //如果设置为忽略处理异常，则不发送系统异常，即不会引发重试动作
                                if(!ignoreException){
                                    new SystemException(SystemException.MQ_MESSAGE_HANDLE_ERROR, e, new Object[]{clazz, methodName}).throwEx();
                                }
                            }

                        }
                    };

                    //设置标识符
                    listener.setListenerId(listenerId);
                    //基于OnEvent注解定义的消费者，其消息都会自动commit
                    EventHandler.getInstance().handleEvent(topic, tag, group, poolSize, listener, false);
                }
            }
        }

        return bean;
    }

    /**
     * 检查实际参数跟方法声明的参数是否精确匹配
     *
     * @param paramterClasses
     * @param params
     * @param realParams
     * @return
     * @author WanWei
     * @date 2015-10-29 上午10:06:01
     */
    private boolean checkParamsTypeMatch(Class<?>[] paramterClasses,
                                         Object[] params, Object[] realParams) {
        if (paramterClasses.length != params.length) {
            return false;
        }
        for (int i = 0; i < paramterClasses.length; i++) {
            Class<?> clazz = null;
            if(params[i] == null) {
                clazz = paramterClasses[i];
            } else {
                clazz = params[i].getClass();
            }
            if (!paramterClasses[i].equals(clazz)) {
                try{
                    paramterClasses[i].cast(params[i]);
                    realParams[i] = params[i];
                    continue;
                }catch(Exception e){
                    log.warn(e.getMessage(), e);
                }
                try {
                    //对基本数据类型的处理
                    clazz = (Class<?>) params[i].getClass().getField("TYPE").get(null);
                    if (!paramterClasses[i].equals(clazz)) {
                        return false;
                    }
                } catch (Exception e) {
                    log.warn(e.getMessage(), e);
                    return false;
                }
            }
            realParams[i] = params[i];
        }

        return true;
    }

    /**
     * 检查方法声明的参数是否与实际参数模糊匹配
     * 模糊匹配只需要方法声明参数能从实际参数中找到对应的参数且先后顺序一致即可
     *
     * @param paramterClasses
     * @param params
     * @param realParams
     * @return
     * @author WanWei
     * @date 2015-11-10 上午10:31:45
     */
    private boolean checkParamsTypeMatchFuzzy(Class<?>[] paramterClasses,
                                              Object[] params, Object[] realParams) {
        //方法声明参数个数不能大于实际参数个数
        if (paramterClasses.length > params.length) {
            return false;
        }
        //用来保存实际提取的参数
        int j = 0;
        for (int i = 0; i < paramterClasses.length; i++) {

            boolean bingo = false;
            for(; j < params.length;j++){
                Class<?> clazz = null;
                if(params[j] == null){
                    clazz = paramterClasses[i];
                }else{
                    clazz = params[j].getClass();
                }
                if (!paramterClasses[i].equals(clazz)) {
                    try{
                        paramterClasses[i].cast(params[j]);
                        bingo = true;
                        break;
                    }catch(Exception e){
                        log.warn(e.getMessage(), e);
                    }
                    try {
                        //对基本数据类型的处理
                        clazz = (Class<?>) params[i].getClass().getField("TYPE").get(null);
                        if (!paramterClasses[i].equals(clazz)) {
                            continue;
                        }
                    } catch (Exception e) {
                        log.warn(e.getMessage(), e);
                        continue;
                    }
                }
                bingo = true;
                break;
            }

            if(!bingo){
                return false;
            }

            realParams[i] = params[j];
            j++;
        }

        return true;
    }

    /**
     * 数据验证
     *
     * @param method
     * @param realParams
     * @throws Exception
     * @author WanWei
     * @date 2015-12-17 上午10:30:41
     */
    private void processValidate(final Method method, Object[] realParams) throws Exception{
        //数据验证
        if(method.isAnnotationPresent(Validation.class)){
            Validation val = method.getAnnotation(Validation.class);
            Class<?> verifyClass = val.verifyClass();
            String verifyMethodStr = val.verifyMethod();
            if(val.useSameMethod()){
                verifyMethodStr = method.getName();
            }
            Method verifyMethod = ToolUtil.findMethod(verifyMethodStr, verifyClass);

            Object bean = ApplicationContextFactory.getBean(verifyClass);
            if(bean == null){
                log.error("方法[{}]标注的注解@Validation中指定的verifyClass需要能被注入至Spring容器中，" +
                        "请为类[{}]添加@Component注解，系统将会跳过该方法的参数检查", method.getName(), verifyClass);
                return;
            }

            if(verifyMethod == null){
                log.error("方法[{}]标注的注解@Validation中指定的verifyClass和verifyMethod不正确，系统将跳过该方法的参数检查");
                return;
            }

            verifyMethod.invoke(bean, realParams);
        }
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }

    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
