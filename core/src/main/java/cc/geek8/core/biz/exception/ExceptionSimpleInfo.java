package cc.geek8.core.biz.exception;

import cc.geek8.core.common.base.BaseModel;

/**
 * 封装简要异常信息
 * 用来在前后台服务之间传递
 *
 * @author WanWei
 * @date 2016-6-29 下午6:09:24
 */
public class ExceptionSimpleInfo extends BaseModel {

    private static final long serialVersionUID = -165919395753142387L;

    //异常码
    private String ecode;

    public String getEcode() {
        return ecode;
    }

    public void setEcode(String ecode) {
        this.ecode = ecode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    //异常描述信息
    private String errorMsg;

}
