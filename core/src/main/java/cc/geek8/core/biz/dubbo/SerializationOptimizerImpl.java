package cc.geek8.core.biz.dubbo;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import com.alibaba.dubbo.common.serialize.support.SerializationOptimizer;

/**
 * 当采用java object作为前后端的传递对象时，可以利用该实现进行对象序列化加速
 *
 * @author WanWei
 * @date 2015-9-9 下午1:31:17
 */
public class SerializationOptimizerImpl implements SerializationOptimizer {

    @SuppressWarnings("rawtypes")
    public Collection<Class> getSerializableClasses() {
        List<Class> classes = new LinkedList<Class>();
        //TODO 当模型规模过大时启用 record by WanWei 2015-09-09
//        classes.add(BidRequest.class);
//        classes.add(User.class);
        return classes;
    }
}
