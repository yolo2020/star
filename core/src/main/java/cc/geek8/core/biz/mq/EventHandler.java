package cc.geek8.core.biz.mq;


import cc.geek8.core.biz.mq.role.MQConsumer;
import cc.geek8.core.biz.mq.service.AplatMQConfig;
import cc.geek8.core.biz.mq.service.AplatMQFactory;
import cc.geek8.core.biz.mq.service.IMQMessageListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 为消息消费者提供的工具类
 *
 * @author WanWei
 * @date 2015-10-28 下午4:58:54
 */
public class EventHandler {

    // 实例
    private static EventHandler handler = new EventHandler();

    // 暂存已经为某些topic创建的consumer提高利用率,后续可以控制consumer个数
    private static Map<String, List<MQConsumer>> consumers = new HashMap<String, List<MQConsumer>>(0);

    /**
     * 单例模式
     */
    private EventHandler() {

    }

    /**
     * 获取实例
     *
     * @return
     * @author WanWei
     * @date 2015-10-28 下午3:51:06
     */
    public static EventHandler getInstance() {
        return handler;
    }

    /**
     * 处理消息
     *
     * @param topic
     * @param tag
     * @param groupId
     * @param handleThread
     * 			处理线程个数，当值小于1时不生效
     * @param listener
     * @param manualCommit
     * 			是否手动提交消息事务
     * @author WanWei
     * @date 2015-10-28 下午7:00:36
     */
    public void handleEvent(String topic, String tag, String groupId, int handleThread, IMQMessageListener listener, boolean manualCommit){
        if((topic == null) || topic.isEmpty()){
            topic = AplatMQConfig.getSystemTopic();
        }
        if((groupId == null) || groupId.isEmpty()){
            groupId = AplatMQConfig.DEFAULT_CONSUMER_GROUP;
        }

        String key = topic.concat(groupId);

        List<MQConsumer> consumerGroup = consumers.get(key);
        if((consumerGroup == null) || consumerGroup.isEmpty()){
            //采用系统配置的zookeeper节点
            consumerGroup = AplatMQFactory.createConsumer(topic, null, null, groupId, handleThread, null, manualCommit);
            consumers.put(key, consumerGroup);
        }
        for(MQConsumer consumer : consumerGroup) {
            consumer.addMessageListener(tag, listener);
            consumer.startReceiving();
        }
    }
}
