package cc.geek8.core.biz.mq;


import cc.geek8.core.biz.mq.role.MQProducer;
import cc.geek8.core.biz.mq.service.AplatMQConfig;
import cc.geek8.core.biz.mq.service.AplatMQFactory;
import cc.geek8.core.biz.mq.service.MQMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * 为消息发送者提供工具类
 *
 * @author WanWei
 * @date 2015-10-28 下午3:48:07
 */
public class EventPublisher {

    // 实例
    private static EventPublisher publisher = new EventPublisher();

    // 暂存已经为某些topic创建的producer提高利用率
    private static Map<String, MQProducer> producers = new HashMap<String, MQProducer>(0);

    /**
     * 单例模式
     */
    private EventPublisher() {

    }

    /**
     * 获取实例
     *
     * @return
     * @author WanWei
     * @date 2015-10-28 下午3:51:06
     */
    public static EventPublisher getInstance() {
        return publisher;
    }

    /**
     * 发送事件
     *
     * @param topic
     *            主题
     * @param tag
     *            过滤的标签
     * @param data
     *            数据
     * @author WanWei
     * @date 2015-10-28 下午3:58:53
     */
    public void sendEvent(String topic, String tag, Object... data) {
        if (topic == null) {
            topic = AplatMQConfig.getSystemTopic();
        }
        // 测试时暂时不发消息
//		if (topic.equals(MQTopicConstants.TOPIC_NOTIFY)){
//			return;
//		}
        MQProducer producer = producers.get(topic);
        if (producer == null) {
            // 采用系统配置的zookeeper节点
            producer = AplatMQFactory.createProducer(topic, null);
            producers.put(topic, producer);
        }

        MQMessage message = new MQMessage(data);
        message.setTag(tag);
        producer.addData(message);
        producer.send();
    }

    /**
     * 发送消息
     *
     * @param topic
     *
     * @param message
     *            消息
     * @author WanWei
     * @date 2015-10-28 下午3:58:53
     */
    public void sendMessage(String topic, MQMessage message) {
        if (topic == null) {
            topic = AplatMQConfig.getSystemTopic();
        }
        MQProducer producer = producers.get(topic);
        if (producer == null) {
            // 采用系统配置的zookeeper节点
            producer = AplatMQFactory.createProducer(topic, null);
            producers.put(topic, producer);
        }

        producer.addData(message);
        producer.send();
    }
}
