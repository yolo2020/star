package cc.geek8.core.biz.mq.role;


import io.jafka.producer.Producer;
import io.jafka.producer.ProducerConfig;
import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.biz.mq.serializer.MQMessageEncoder;
import cc.geek8.core.biz.mq.service.AplatMQConfig;
import cc.geek8.core.biz.mq.service.MQMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * 消息生产者
 *
 * @author WanWei
 * @date 2015-10-27 下午3:15:52
 */
public class MQProducer {

    private static Logger log = LoggerFactory.getLogger(MQProducer.class);

    // 基于Jafka MQ的生产者
    private Producer<String, MQMessage> producer = null;

    // 用来封装MQMessage
    private MQMessageProducerData data = null;

    // 若发送消息失败则重试，默认重试3次
    private static final int RETRY_COUNT = 3;

    // 第一次重试的时间间隔，并不会立刻重试
    private static final long FIRST_RETRY_INTERVAL_MS = 200;
    /**
     * 基于指定的topic和 Zookeeper连接来创建生产者
     *
     * @param topic
     * @param zkConnect
     */
    public MQProducer(String topic, String zkConnect) {
        // zkConnect若为null，则采用配置文件中设定的值
        if (zkConnect == null) {
            zkConnect = AplatMQConfig.getConfigValue(AplatMQConfig.ZOOKEEPER_CONNECT);
        }
        // 如果topic为null，则使用系统默认topic
        if ((topic == null) || topic.isEmpty()) {
            topic = AplatMQConfig.getSystemTopic();
        }
        Properties props = new Properties();
        props.put(AplatMQConfig.ZOOKEEPER_CONNECT, zkConnect);
        props.put(AplatMQConfig.SERIALIZER_CLASS, MQMessageEncoder.class.getName());
        props.put(AplatMQConfig.ZK_CONNECTION_TIMEOUT_MS,
                AplatMQConfig.getConfigValue(AplatMQConfig.ZK_CONNECTION_TIMEOUT_MS));
        ProducerConfig config = new ProducerConfig(props);
        producer = new Producer<String, MQMessage>(config);
        data = new MQMessageProducerData(topic);
    }

    /**
     * 添加数据，可多次添加
     *
     * @param message
     * @author WanWei
     * @date 2015-10-27 下午4:44:49
     */
    public void addData(MQMessage message) {
        data.add(message);
    }

    /**
     * 发送添加到生产者的数据 发送完后数据会清空
     * 若失败会按照默认规则重试，重试规则不可配置
     *
     * @author WanWei
     * @date 2015-10-27 下午4:46:22
     */
    public void send() {
        boolean retry = false;
        int count = 0;
        long retryInterval = FIRST_RETRY_INTERVAL_MS;
        Exception ex = null;
        do {
            if(count == RETRY_COUNT) {
                break;
            }

            try {
                if(count > 0) {
                    Thread.sleep(retryInterval);
                }
                producer.send(data);
                data.getData().clear();
                retry = false;
            } catch (Exception e) {
                count++;
                ex = e;
                log.error("发送MQ消息失败，系统将在{}毫秒后重试，共重试{}次，将开始第{}次重试", retryInterval, RETRY_COUNT, count, e);
                retry = true;
                retryInterval = retryInterval * (count + 1);

            }
        } while(retry);
        if((count == RETRY_COUNT) && retry) {
            new SystemException(SystemException.ERROR_MQ_SEND_MESSAGE, ex).throwEx();
        }

    }

}
