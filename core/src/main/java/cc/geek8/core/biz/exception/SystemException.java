package cc.geek8.core.biz.exception;
import cc.geek8.core.common.i18n.annotation.MessageCN;

/**
 * 发生IOException、IllegalArgumentException或其他非逻辑异常时
 * 封装的业务异常类
 *
 * @author WanWei
 * @date 2015-10-13 下午4:58:12
 */
public class SystemException extends BaseBizException {

    private static final long serialVersionUID = 3583075680220452251L;

    //模块ID
    private static final String ID_SYSTEM = "SYS";

    @MessageCN("未知类型的业务编号:{0}, 系统无法从该业务编号中提取ID")
    public static final String UNKNOWN_BIZ_NO_TYPE = createExceptionCode(1);

    @MessageCN("MQConsumer[{0}]没有消息处理类，无法接收APlat-MQ[{1}]产生的消息")
    public static final String MQ_CONSUMER_NO_LISTENER = createExceptionCode(2);

    @MessageCN("{0}-{1} 处理消息出错")
    public static final String MQ_MESSAGE_HANDLE_ERROR = createExceptionCode(3);

    @MessageCN("序列化失败，相关实体：{0}-{1}")
    public static final String SERIALIZE_ENCODE_ERROR = createExceptionCode(4);

    @MessageCN("反序列化失败，相关类：{0}")
    public static final String SERIALIZE_DECODE_ERROR = createExceptionCode(5);

    @MessageCN("Object[{0}]转json字符串失败")
    public static final String JSON_OBJ_TO_STRING_ERROR = createExceptionCode(6);

    @MessageCN("Json字符串转Object[{0}]失败, Json：{1}")
    public static final String JSON_STRING_TO_OBJ_ERROR = createExceptionCode(7);

    @MessageCN("Json字符串转List<{0}>失败, Json：{1}")
    public static final String JSON_STRING_TO_LIST_ERROR = createExceptionCode(8);

    @MessageCN("Json字符串转复杂类型[{0}]失败, Json：{1}")
    public static final String JSON_STRING_TO_COMPLEX_OBJ_ERROR = createExceptionCode(9);

    @MessageCN("RPC调用时，传递的参数不合法，未通过非null或非空检查")
    public static final String RPC_CALL_PATAMETER_ERROR = createExceptionCode(10);

    @MessageCN("数据库操作错误，请检查SQL和数据表的正确性")
    public static final String DB_OPERATION_ERROR = createExceptionCode(11);

    @MessageCN("未配置系统邮箱[system.mail]，无法发送邮件")
    public static final String SYSTEM_MAIL_UNKNOWN_ERROR = createExceptionCode(12);

    @MessageCN("参数验证方法调用失败，出现未知错误")
    public static final String VALIDATION_INVOKE_UNKNOWN_ERROR = createExceptionCode(13);

    @MessageCN("数据验证失败 - 表达式[{0}]必须为true")
    public static final String VALIDATION_IS_TRUE_ERROR = createExceptionCode(14);

    @MessageCN("数据验证失败 - 参数[{0}]必须为null")
    public static final String VALIDATION_IS_NULL_ERROR = createExceptionCode(15);

    @MessageCN("数据验证失败 - 参数[{0}]不能为null")
    public static final String VALIDATION_NOT_NULL_ERROR = createExceptionCode(16);

    @MessageCN("数据验证失败 - 参数[{0}]不能为空或null")
    public static final String VALIDATION_HAS_LENGTH_ERROR = createExceptionCode(17);

    @MessageCN("数据验证失败 - 集合[{0}]不能为空，其必须至少含有1个元素")
    public static final String VALIDATION_NOT_EMPTY_ERROR = createExceptionCode(18);

    @MessageCN("数据验证失败 - 集合[{0}]不能包含任何null元素")
    public static final String VALIDATION_NO_NULL_ELEMENT_ERROR = createExceptionCode(19);

    @MessageCN("数据验证失败 - 对象[{0}]必须是类[{1}]的实例")
    public static final String VALIDATION_INSTANCEOF_ERROR = createExceptionCode(20);

    @MessageCN("数据验证失败 - 对象实体属性校验情况：{0}")
    public static final String VALIDATION_OVAL_CHECK_ERROR = createExceptionCode(21);

    @MessageCN("短信平台故障")
    public static final String SMS_PLATFORM_ERROR = createExceptionCode(22);

    @MessageCN("短信发送失败:{0}")
    public static final String SMS_SEND_FAILED = createExceptionCode(23);

    @MessageCN("数据字典实例缺失，缓存中不存在key为{0}的字典数据")
    public static final String ERROR_DICTIONARY_NOTIN_CACHE = createExceptionCode(24);

    @MessageCN("从email模板{0}中提取内容失败")
    public static final String ERROR_GET_INFO_FROM_MAILTEMPLATE = createExceptionCode(25);

    @MessageCN("向(0)发送邮件失败")
    public static final String ERROR_EMAIL_SEND_FAILED = createExceptionCode(26);

    @MessageCN("添加信息至消息生产者失败")
    public static final String ERROR_ADD_MSG_TO_MQPRODUCTER = createExceptionCode(27);

    @MessageCN("MQ消息发送失败")
    public static final String ERROR_MQ_SEND_MESSAGE = createExceptionCode(28);

    @MessageCN("创建Lucene文件索引查询器失败，资源类型:{0}")
    public static final String ERROR_CREATE_FILESEARCHER_LUCENEINDEX = createExceptionCode(29);

    @MessageCN("创建Lucene内存索引失败，资源类型:{0}")
    public static final String ERROR_CREATE_RAM_LUCENEINDEX = createExceptionCode(30);

    @MessageCN("添加索引至Lucene失败，资源类型:{0} 资源主键:{1}")
    public static final String ERROR_ADD_DOC_TO_LUCENE = createExceptionCode(31);

    @MessageCN("从Lucene删除索引失败，资源类型:{0} 资源主键:{1}")
    public static final String ERROR_DEL_DOC_TO_LUCENE = createExceptionCode(32);

    @MessageCN("更新索引至Lucene失败，资源类型:{0} 资源主键:{1}")
    public static final String ERROR_UPDATE_DOC_TO_LUCENE = createExceptionCode(33);

    @MessageCN("创建Lucene内存索引查询器失败，资源类型:{0}")
    public static final String ERROR_CREATE_LUCENE_READER = createExceptionCode(34);

    @MessageCN("添加索引-事件参数错误")
    public static final String ERROR_ADDINDEX_PARAMS = createExceptionCode(35);

    @MessageCN("更新索引-事件参数错误")
    public static final String ERROR_UPDATEINDEX_PARAMS = createExceptionCode(36);

    @MessageCN("删除索引-事件参数错误")
    public static final String ERROR_DELINDEX_PARAMS = createExceptionCode(37);

    @MessageCN("从lucene中查询结果失败，关键字:{0} 资源类型:{1}")
    public static final String ERROR_SEARCH_INDEX_FROM_LUCENE = createExceptionCode(38);

    @MessageCN("刷新Lucene查询器失败")
    public static final String ERROR_REFRESH_LUCENE_SEARCHER = createExceptionCode(39);

    @MessageCN("Lucene内存索引与文件索引不匹配，资源类型:{0}")
    public static final String ERROR_LUCENE_FILE_RAM_UNSYNC = createExceptionCode(40);

    @MessageCN("创建Lucene文件索引写入器失败，资源类型:{0}")
    public static final String ERROR_CREATE_FILEWRITER_LUCENEINDEX = createExceptionCode(41);

    @MessageCN("Lucene内存索引合并至文件索引失败，资源类型编号:{0}")
    public static final String ERROR_LUCENE_MERGEINDEX = createExceptionCode(42);

    @MessageCN("quartz调度移除触发器失败")
    public static final String REMOVE_TRIGGER = createExceptionCode(43);

    @MessageCN("quartz调度重启触发器失败")
    public static final String RESUME_TRIGGER = createExceptionCode(44);

    @MessageCN("quartz调度暂停触发器失败")
    public static final String PAUSE_TRIGGER = createExceptionCode(45);

    @MessageCN("quartz调度移除任务失败")
    public static final String REMOVE_JOB = createExceptionCode(46);

    @MessageCN("quartz调度重启任务失败")
    public static final String RESUME_JOB = createExceptionCode(47);

    @MessageCN("quartz调度暂停任务失败")
    public static final String PAUSE_JOB = createExceptionCode(48);

    @MessageCN("创建MQ生产者失败，topic:{0}, namesrv:{1}")
    public static final String ERROR_MQ_CREATE_PRODUCER = createExceptionCode(50);

    @MessageCN("创建MQ生产者失败，topic:{0}, namesrv:{1}, threadSize:{3}")
    public static final String ERROR_MQ_CREATE_CONSUMER = createExceptionCode(51);

    @MessageCN("消息序列化失败,系统无法处理")
    public static final String ERROR_MQ_MESSAGE_ENCODER = createExceptionCode(52);

    @MessageCN("消息对象反序列化失败，系统无法处理")
    public static final String ERROR_MQ_MESSAGE_DECODER = createExceptionCode(53);

    @MessageCN("消息消费者订阅Topic识别，topic:{0}")
    public static final String ERROR_MQ_CONSUMER_SUBSCRIBE = createExceptionCode(54);

    @MessageCN("消息消费者启动失败，topic:{0}")
    public static final String ERROR_MQ_CONSUMER_START = createExceptionCode(55);

    @MessageCN("用户{0}未通过认证，拒绝访问")
    public static final String USER_UNAUTH_ERROR = createExceptionCode(100);

    @MessageCN("用户[{0}]Session过期，拒绝访问")
    public static final String USER_SESSION_EXPIRED_ERROR =  createExceptionCode(101);

    @MessageCN("账号[{0}]在其他地方登录")
    public static final String USER_LOGIN_ALREADY_ERROR = createExceptionCode(102);

    @MessageCN("从lucene查询中获取主键列表失败，关键字:{0} 资源类型:{1}")
    public static final String ERROR_SEARCH_PRIMARYKEY_FROM_LUCENE = createExceptionCode(103);

    @MessageCN("Lucene中根据ID查询索引失败，ID:{0} 资源类型:{1}")
    public static final String ERROR_SEARCH_ID_FROM_LUCENE = createExceptionCode(104);

    @MessageCN("系统未知错误")
    public static final String CATCH_UNKNOWN_EXCEPTION = createExceptionCode(105);

    @MessageCN("手机号位数错误")
    public static final String MOBILE_NO_SIZE = createExceptionCode(106);

    @MessageCN("手机号不符合国家规范")
    public static final String NOT_MOBILE = createExceptionCode(107);

    @MessageCN("身份证位数错误")
    public static final String CARDID_NO_SIZE = createExceptionCode(108);

    @MessageCN("18位身份证号码不符合国家规范")
    public static final String NOT_CARDID = createExceptionCode(109);

    @MessageCN("系统未知错误")
    public static final String CATCH_UNKNOWN_EXCEPTION_1 = createExceptionCode(110);

    @MessageCN("港澳通行证不符合国家规范")
    public static final String HKCardId_NO_HKCARDID = createExceptionCode(111);

    @MessageCN("港澳通行证位数错误")
    public static final String HKCARDID_NO_SIZE = createExceptionCode(112);

    @MessageCN("台胞证不符合国家规范")
    public static final String TWCARDID_NO_TWCARDID = createExceptionCode(113);

    @MessageCN("台胞证位数错误")
    public static final String TWCARDID_NO_SIZE = createExceptionCode(114);

    @MessageCN("护照不符合国家规范")
    public static final String PASSPORTCARDID_NO_TWCARD = createExceptionCode(115);

    @MessageCN("护照位数错误")
    public static final String TWCARDID_NO_PASSPORTCARDID = createExceptionCode(116);

    @MessageCN("Java对象序列化失败，无法存入Redis")
    public static final String REDIS_OBJECT_SERIALIZE_ERROR = createExceptionCode(117);

    @MessageCN("Java对象反序列化失败，无法从Redis读取")
    public static final String REDIS_OBJECT_DESERIALIZE_ERROR = createExceptionCode(118);



    public SystemException() {

    }

    public SystemException(String code, Exception original, Object... params) {
        super(code, original, params);
    }

    /**
     * 生产异常码
     * @param code
     * @return
     * @author WanWei
     * @date 2015-10-13 下午5:04:09
     */
    private static String createExceptionCode(int code) {
        return getFormatedNumber(ID_SYSTEM, code);
    }

}
