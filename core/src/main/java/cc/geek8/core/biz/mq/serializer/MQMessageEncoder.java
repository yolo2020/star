package cc.geek8.core.biz.mq.serializer;

import de.ruedigermoeller.serialization.FSTObjectOutput;
import io.jafka.message.Message;
import io.jafka.producer.serializer.Encoder;
import cc.geek8.core.biz.mq.service.MQMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 对MQMessage进行序列化处理
 *
 * @author WanWei
 * @date 2015-10-27 下午2:42:26
 */
public class MQMessageEncoder implements Encoder<MQMessage> {

    private static Logger log = LoggerFactory.getLogger(MQMessageEncoder.class);

    /**
     * 构造函数 设置序列化工具
     */
    public MQMessageEncoder() {

    }

    /**
     * 执行序列化
     */
    public Message toMessage(MQMessage event) {
        byte[] bytes = null;
        ByteArrayOutputStream baos = null;
        FSTObjectOutput out = null;
        try {
            baos = new ByteArrayOutputStream();
            out = new FSTObjectOutput(baos);
            out.writeObject(event, MQMessage.class);
            out.flush();

            bytes = baos.toByteArray();
        } catch(IOException ioe){
            log.error("消息序列化失败,系统无法处理", ioe);
            return null;
        } finally {
            try {
                if(out != null){
                    out.close();
                }
                if(baos != null){
                    baos.close();
                }
            } catch (Exception e) {
                // 发生此异常不会影响系统运行
                log.error("关闭字节流错误", e);
            }
        }

        return new Message(bytes);
    }
}
