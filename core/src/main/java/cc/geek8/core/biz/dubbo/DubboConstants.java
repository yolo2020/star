package cc.geek8.core.biz.dubbo;

/**
 * Dubbox框架本身用到的一些常量
 *
 * @author WanWei
 * @date 2016-5-12 下午12:19:41
 */
public interface DubboConstants {

    //自定义的客户端filter名
    String FILTER_CLIENT = "aplatclient";

    //自定义的服务端filter名
    String FILTER_SERVER = "aplatserver";

    String FILTER_EXCEPTION = "bizexception";

    //RPC调用超时时间为2秒
    int TIME_OUT = 10000;
}
