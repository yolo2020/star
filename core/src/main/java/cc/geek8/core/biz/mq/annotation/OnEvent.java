package cc.geek8.core.biz.mq.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识哪些方法可以处理消息
 *
 * @author WanWei
 * @date 2015-10-28 下午4:46:46
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OnEvent {

    //主题名称
    String topic() default "";

    //过滤标签
    String tag() default "";

    //组别
    String group() default "";

    //处理事件的线程池大小， 默认值为1
    int poolSize() default 1;

    //忽略处理方法产生的异常 默认为false 当出现异常后会不断重试
    boolean ignoreException() default false;//Add by WanWei 2016-01-25 当前系统bug较多，避免由bug导致问题而不断重复

    //参数模糊匹配 默认是false 即精确匹配
    boolean fuzzyMatch() default false;
}
