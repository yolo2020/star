package cc.geek8.core.biz.session;
import cc.geek8.core.common.base.BaseModel;
import cc.geek8.core.common.base.CommonConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 包装用户session信息的模型
 *
 * @author WanWei
 * @date 2017年6月30日 下午4:48:02
 */
public class SessionInfo extends BaseModel {

    private static final long serialVersionUID = 4118364021992611756L;

    private String userAccount;

    private String userName;

    //请求者IP
    private String requestIP;

    private String sessionCode;

    //用户自定义信息
    //private Map<String, String> bizInfo = new HashMap<String, String>();
    private Map<String, String> bizInfo = new HashMap<String, String>();

    private Map<Integer, List<String>> roleInfoMap = new HashMap<>();

    private Integer currentRoleId;

    //用户的直属机构
    private Integer userOrg;

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public Map<String, String> getBizInfo() {
        return bizInfo;
    }

    public void setBizInfo(Map<String, String> bizInfo) {
        this.bizInfo = bizInfo;
    }

    public String getRequestIP() {
        return requestIP;
    }

    public void setRequestIP(String requestIP) {
        this.requestIP = requestIP;
    }
    /**
     * 将数据转换成map形式
     *
     * @return
     * @author WanWei
     * @date 2017年7月10日 下午2:23:30
     */
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<String, String>(bizInfo);
        map.put(CommonConstants.USER_ACCOUNT, getUserAccount());
        map.put(CommonConstants.USER_NAME, getUserName());
        map.put(CommonConstants.USER_IP, getRequestIP());
        map.put(CommonConstants.SESSION_CODE, getSessionCode());
        map.put("user_org", getUserOrg().toString());
        return map;
    }

    public Integer getUserOrg() {
        return userOrg;
    }

    public void setUserOrg(Integer userOrg) {
        this.userOrg = userOrg;
    }

    public Map<Integer, List<String>> getRoleInfoMap() {
        return roleInfoMap;
    }

    public void setRoleInfoMap(Map<Integer, List<String>> roleInfoMap) {
        this.roleInfoMap = roleInfoMap;
    }

    public Integer getCurrentRoleId() {
        return currentRoleId;
    }

    public void setCurrentRoleId(Integer currentRoleId) {
        this.currentRoleId = currentRoleId;
    }
}
