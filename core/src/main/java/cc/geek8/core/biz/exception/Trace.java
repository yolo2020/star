package cc.geek8.core.biz.exception;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;


/**
 * 工具类，提取异常相关信息
 *
 * @author WanWei
 * @date 2014-09-08 下午2:54:46
 */
public class Trace {

    /**
     * 获取指定编码的错误堆栈信息
     *
     * @param e
     *
     * @author WanWei
     * @date 2014-9-8  下午4:27:31
     */
    public static String getStackTrace(Exception e) {
        String rst = "";
        if (null == e){
            return rst;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(os, true);
        e.printStackTrace(pw);
        try {
            rst = os.toString("utf-8");
        } catch (UnsupportedEncodingException ue) {
            rst = os.toString();
        }
        return rst;
    }
}
