package cc.geek8.core.biz.validation.aspect;


import cc.geek8.core.biz.exception.BaseBizException;
import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.biz.validation.annotation.Validation;
import cc.geek8.core.common.spring.ApplicationContextFactory;
import cc.geek8.core.utils.ToolUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * 使用Spring AOP技术对需要做参数校验的方法进行切点
 *
 * @author WanWei
 * @date 2015-12-16 下午1:45:03
 */
@Aspect
@Component
@Service
public class ValidationAspect {

    private static Logger log = LoggerFactory.getLogger(ValidationAspect.class);

    /**
     * 标注了@Validation的方法添加切点
     *
     * @author WanWei
     * @date 2015-10-13 下午4:40:06
     */
    @Pointcut("@annotation(net.fullwisdom.core.biz.validation.annotation.Validation)")
    public void validationAspect() {

    }

    /**
     * 具体业务方法调用之前，进入本方法执行参数验证
     *
     * @param jp
     * @author WanWei
     * @date 2015-12-16 下午1:48:28
     */
    @Before("validationAspect()")
    public void beforeMethodCall(JoinPoint jp) {
        String currentMethodName = jp.getSignature().getName();
        Class<?> target = jp.getTarget().getClass();
        Method currentMethod = ToolUtil.findMethod(currentMethodName, target);

        if (currentMethod == null) {
            return;
        }

        if (currentMethod.isAnnotationPresent(Validation.class)) {
            Validation val = currentMethod.getAnnotation(Validation.class);
            Class<?> verifyClass = val.verifyClass();

            // @Validation需要跟Spring的@Component配合使用
            Object bean = ApplicationContextFactory.getBean(verifyClass);
            if (bean == null) {
                log.error("方法[{}]标注的注解@Validation中指定的verifyClass需要能被注入至Spring容器中，"
                        + "请为类[{}]添加@Component注解，系统将会跳过该方法的参数检查", currentMethodName, verifyClass);
                return;
            }

            String verifyMethodName = val.verifyMethod();
            if (val.useSameMethod()) {
                verifyMethodName = currentMethodName;
            }

            Method verifyMethod = ToolUtil.findMethod(verifyMethodName, verifyClass);
            if (verifyMethod == null) {
                log.error("方法[{}]标注的注解@Validation中指定的verifyClass和verifyMethod不正确，系统将跳过该方法的参数检查");
                return;
            }

            // 具体做参数检查的方法与被检查的方法的参数必须保持一致
            if (!Arrays.equals(verifyMethod.getParameterTypes(), currentMethod.getParameterTypes())) {
                log.error("verifyMethod[{}]与被检查方法[{}]的参数不匹配，系统将跳过该方法的参数检查", verifyMethodName, currentMethodName);
                return;
            }
            try {
                verifyMethod.invoke(bean, jp.getArgs());
            } catch (InvocationTargetException e) {
                Throwable realException = e.getTargetException();
                if (realException instanceof BaseBizException) {
                    throw (BaseBizException) realException;
                }
                log.error("调用验证方法失败，发生InvocationTargetException错误", e);
                // 抛出业务异常
                new SystemException(SystemException.VALIDATION_INVOKE_UNKNOWN_ERROR, e).throwEx();
            } catch (Exception el) {
                log.error("调用验证方法失败，出现了非预期错误", el);
                // 抛出业务异常
                new SystemException(SystemException.VALIDATION_INVOKE_UNKNOWN_ERROR, el).throwEx();
            }

        }

    }

}
