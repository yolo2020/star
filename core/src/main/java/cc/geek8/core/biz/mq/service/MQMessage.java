package cc.geek8.core.biz.mq.service;


import java.io.Serializable;
import java.util.UUID;

/**
 * 用来在MQ传递的消息实体
 * @author WanWei
 * @date 2015-10-27 下午4:30:35
 */
public class MQMessage implements Serializable {

    private static final long serialVersionUID = 4486491289332571226L;

    //消息ID，在初始化时自动生成
    private String id;

    //标签关键字，用来过滤消息，可以为null
    private String tag;

    //可序列化的数据
    private Serializable data;

    public MQMessage(){
        id = UUID.randomUUID().toString();
    }

    public MQMessage(Serializable obj){
        this();
        this.data = obj;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Serializable getData() {
        return data;
    }

    public void setData(Serializable data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
