package cc.geek8.core.biz.mq.role;


import io.jafka.producer.ProducerData;
import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.biz.mq.service.MQMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * 用来封装MQMessage的数据模型
 *
 * @author WanWei
 * @date 2015-10-27 下午2:23:11
 */
public class MQMessageProducerData extends ProducerData<String, MQMessage> {

    public MQMessageProducerData(String topic, String key, List<MQMessage> data) {
        super(topic, key, data);
    }

    public MQMessageProducerData(String topic, List<MQMessage> data) {
        super(topic, data);
    }

    public MQMessageProducerData(String topic, MQMessage data) {
        super(topic, data);
    }

    public MQMessageProducerData(String topic) {
        this(topic, new ArrayList<MQMessage>());
    }

    public MQMessageProducerData add(MQMessage message) {
        if(getData().add(message)) {
            return this;
        }

        //添加消息失败后抛出业务异常
        new SystemException(SystemException.ERROR_ADD_MSG_TO_MQPRODUCTER, null).throwEx();
        return null;
    }
}
