package cc.geek8.core.biz.mq.service;

import cc.geek8.core.biz.mq.MQTopicConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 用来获取消息中间件配置信息的实体
 *
 * @author WanWei
 * @date 2015-10-27 下午3:20:27
 */
public class AplatMQConfig implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(AplatMQConfig.class);

    private static final long serialVersionUID = -6641865932408316638L;

    // Zookeeper的连接信息
    public static final String ZOOKEEPER_CONNECT = "zk.connect";

    // 序列化类名
    public static final String SERIALIZER_CLASS = "serializer.class";

    // 消费者分组，规则：一个消息只能被属于同一个组的一个消费者消费，能够被属于不同组的多个消费者消费
    public static final String CONSUMER_GROUPID = "groupid";

    // 消费者ID,非必输项
    public static final String CONSUMER_ID = "consumerid";

    // ZooKeeper连接超时时间
    public static final String ZK_CONNECTION_TIMEOUT_MS = "zk.connectiontimeout.ms";

    // 默认用户分组, 'push'的MD5码
    public static final String DEFAULT_CONSUMER_GROUP = "21a361d96e3e13f5f109748c2a9d2434";

    //默认topic
    public static final String DEFAULT_SYSTEM_TOPIC = "default.system.topic";

    // 配置内容
    private static Map<String, String> config = new HashMap<String, String>(0);

    private static String defaultSystemTopic ;

    public AplatMQConfig() {
    }

    public AplatMQConfig(Map<String, String> map) {
        config.putAll(map);
    }

    /**
     * 获取配置项的值
     *
     * @param key
     * @return
     * @author WanWei
     * @date 2015-10-27 下午4:17:29
     */
    public static String getConfigValue(String key) {
        return config.get(key);
    }

    public static String getSystemTopic() {
        if (StringUtils.isEmpty(defaultSystemTopic)) {

            try {
                defaultSystemTopic = getConfigValue(DEFAULT_SYSTEM_TOPIC);
            } catch (Exception e) {
                log.warn("failed to get system.name from config.use default topic:"
                        + MQTopicConstants.TOPIC_DEFAULT_SYSTEM_TOPIC, e);
            } finally {
                if (StringUtils.isEmpty(defaultSystemTopic)) {
                    defaultSystemTopic = MQTopicConstants.TOPIC_DEFAULT_SYSTEM_TOPIC;
                }
            }
        }
        return defaultSystemTopic;
    }

}
