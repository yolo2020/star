package cc.geek8.core.biz.cache.api;


import java.util.List;

/**
 * 由dubbox对外提供的分布式缓存接口
 * 同一进程内的调用者可以本地调用的方式访问
 *
 * @author WanWei
 * @date 2015年9月10日
 */
public interface DCacheService {

    /**
     * 判断某键值为key的元素 是否存在指定的缓存中
     * 若存在返回true
     * @param cacheName
     * @param key
     * @return
     * @author WanWei
     * @date 2015年9月10日
     */
    public boolean isInCache(String cacheName, String key);

    /**
     * 通过指定键值key从某缓存中取对应的缓存值
     * 可能返回null
     * @param cacheName
     * @param key
     * @return
     * @author WanWei
     * @date 2015年9月10日
     */
    public Object get(String cacheName, String key);

    /**
     * 将某一键值对存入指定缓存
     * @param cacheName
     * @param key
     * @param value
     * @author WanWei
     * @date 2015年9月10日
     */
    public void put(String cacheName, String key, Object value);

    /**
     * 将信息添加到具有有效期的缓存
     * @param cacheName
     * @param key
     * @param value
     * @param  timeSeconds
     * 			过期时间
     * @author WanWei
     * @date 2016-5-24 下午4:05:01
     */
    public void putInCacheExpiry(String cacheName, String key, Object value, long timeSeconds);

    /**
     * 从有过期设置的缓存里取值
     *
     * @author WanWei
     * @date 2019-03-13 8:49 PM
     * @param cacheName, key
     * @return Object
     **/
    public Object getInCacheExpiry(String cacheName, String key);

    /**
     * 从某一缓存中移除指定key值的缓存内容
     * @param cacheName
     * @param key
     * @author WanWei
     * @date 2015年9月10日
     */
    public void remove(String cacheName, String key);

    /**
     * 删除某个缓存
     *
     * @param cacheName
     * @author WanWei
     * @date 2015-10-29 下午4:21:05
     */
    public void remove(String cacheName);

    /**
     * 获取指定cacheName下的所有key值
     * @param cacheName
     * @return List
     * @author WanWei
     * @date 2015年9月16日
     */
    public List<Object> getKeys(String cacheName);
}
