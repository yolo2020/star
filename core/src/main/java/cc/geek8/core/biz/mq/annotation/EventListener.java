package cc.geek8.core.biz.mq.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该标识用来表示某个类实现了消息处理方法
 * 需要跟Spring的@Component注解配合使用
 *
 * @author WanWei
 * @date 2015-10-28 下午3:08:14
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EventListener {

}
