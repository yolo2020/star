package cc.geek8.core.biz.validation;


import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.utils.CardIdUtils;
//import net.sf.oval.ConstraintViolation;
//import net.sf.oval.Validator;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 基于org.springframework.util.Assert的思路重写的断言工具类
 *
 * @author WanWei
 * @date 2015-12-16 下午2:58:30
 */
public class Assert {

    //基于第三方数据验证框架Oval的工具类
   // private static Validator validator = new Validator();

    /**
     * 检查表达式是否为true
     *
     * @param expression
     * @param expressionStr
     * @author WanWei
     * @date 2015-12-16 下午3:53:52
     */
    public static void isTrue(boolean expression, String expressionStr) {
        if (!expression){
            new SystemException(SystemException.VALIDATION_IS_TRUE_ERROR, null, new Object[]{expressionStr}).throwEx();
        }
    }

    /**
     * 检查表达式是否为true
     *
     * @param expression
     * @author WanWei
     * @date 2015-12-16 下午4:01:38
     */
    public static void isTrue(boolean expression) {
        if (!expression){
            new SystemException(SystemException.VALIDATION_IS_TRUE_ERROR, null, new Object[]{""}).throwEx();
        }
    }

    /**
     * 检查对象是否为null
     * @param object
     * @param parameterName
     * 			对象名称
     * @author WanWei
     * @date 2015-12-16 下午4:01:52
     */
    public static void isNull(Object object, String parameterName) {
        if (object != null){
            new SystemException(SystemException.VALIDATION_IS_NULL_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查对象是否为null
     * @param object
     * @param parameterName
     * 			对象名称
     * @author WanWei
     * @date 2015-12-16 下午4:03:35
     */
    public static void notNull(Object object, String parameterName) {
        if (object == null){
            new SystemException(SystemException.VALIDATION_NOT_NULL_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查字符串是否为空
     *
     * @param text
     * @param parameterName
     * 			用来代表字符串的名称
     * @author WanWei
     * @date 2015-12-16 下午4:03:55
     */
    public static void hasLength(String text, String parameterName) {
        if (!StringUtils.hasLength(text)){
            new SystemException(SystemException.VALIDATION_HAS_LENGTH_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查对象数组是否为空
     *
     * @param array
     * @param parameterName
     * 			数组名称
     * @author WanWei
     * @date 2015-12-16 下午4:04:55
     */
    public static void notEmpty(Object[] array, String parameterName) {
        if (ObjectUtils.isEmpty(array)){
            new SystemException(SystemException.VALIDATION_NOT_EMPTY_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查对象数组是否不包含null元素
     *
     * @param array
     * @param parameterName
     * 			数组名称
     * @author WanWei
     * @date 2015-12-16 下午4:05:37
     */
    public static void noNullElements(Object[] array, String parameterName) {
        if (array != null){
            for (Object element : array){
                if (element == null){
                    new SystemException(SystemException.VALIDATION_NO_NULL_ELEMENT_ERROR, null, new Object[]{parameterName}).throwEx();
                }
            }
        }
    }

    /**
     * 检查集合是否为空
     *
     * @param collection
     * @param parameterName
     * 			数组名称
     * @author WanWei
     * @date 2015-12-16 下午4:06:50
     */
    @SuppressWarnings("rawtypes")
    public static void notEmpty(Collection collection, String parameterName) {
        if (CollectionUtils.isEmpty(collection)){
            new SystemException(SystemException.VALIDATION_NOT_EMPTY_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查Map是否为空
     *
     * @param map
     * @param parameterName
     * 			Map名称
     * @author WanWei
     * @date 2015-12-16 下午4:07:29
     */
    @SuppressWarnings("rawtypes")
    public static void notEmpty(Map map, String parameterName) {
        if (CollectionUtils.isEmpty(map)){
            new SystemException(SystemException.VALIDATION_NOT_EMPTY_ERROR, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 检查对象是否是指定类的实例
     *
     * @param type
     * @param obj
     * @param parameterName
     * 			对象名称
     * @author WanWei
     * @date 2015-12-16 下午4:08:12
     */
    public static void isInstanceOf(Class<?> type, Object obj, String parameterName) {
        notNull(type, parameterName);
        if (!type.isInstance(obj)){
            new SystemException(SystemException.VALIDATION_INSTANCEOF_ERROR, null, new Object[]{parameterName, type.getName()}).throwEx();
        }
    }

    /**
     * 基于Oval注解，对实体进行检查
     *
     * @param obj
     * @author WanWei
     * @date 2015-12-16 下午4:09:04
     */

    /*
    public static void ovalValidate(Object obj){
        List<ConstraintViolation> violations = validator.validate(obj);

        if(violations.size() > 0) {
            StringBuffer errorMessage = new StringBuffer();
            for(ConstraintViolation cv : violations){
                errorMessage.append(cv.getMessage());
                errorMessage.append("\n");
            }

            new SystemException(SystemException.VALIDATION_OVAL_CHECK_ERROR, null, new Object[]{errorMessage.toString()}).throwEx();
        }
    }*/

    /**
     * 验证手机号
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 14:20
     */
    public static void validateMobile(String mobile, String parameterName) {
        Assert.notNull(mobile, parameterName);
        boolean isMatch = false;
        if (mobile.length() == 11) {
            String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(mobile);
            isMatch = m.matches();
            if (!isMatch) {
                new SystemException(SystemException.NOT_MOBILE, null, new Object[]{parameterName}).throwEx();
            }
        } else {
            new SystemException(SystemException.MOBILE_NO_SIZE, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 验证身份证
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 14:18
     */
    public static void validateCardId(String cardId, String parameterName) {
        Assert.notNull(cardId, parameterName);
        boolean isMatch = false;
        if (cardId.length() == 18) {
            isMatch = CardIdUtils.cardIdVerify(cardId);
            if (!isMatch) {
                new SystemException(SystemException.NOT_CARDID, null, new Object[]{parameterName}).throwEx();
            }
        } else {
            new SystemException(SystemException.CARDID_NO_SIZE, null, new Object[]{parameterName}).throwEx();
        }
    }

    /**
     * 验证港澳通行证
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 14:18
     */
    public static void validateHKCardId(String cardId, String parameterName) {
        Assert.notNull(cardId, parameterName);
        if (cardId.length() == 9) {
            if (!CardIdUtils.hkCardVerify(cardId)) {
                new SystemException(SystemException.HKCardId_NO_HKCARDID, null, new Object[]{parameterName}).throwEx();
            }
        } else {
            new SystemException(SystemException.HKCARDID_NO_SIZE, null, new Object[]{parameterName}).throwEx();
        }

    }

    /**
     * 验证台湾通行证
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 14:18
     */
    public static void validateTWCardId(String cardId, String parameterName) {
        Assert.notNull(cardId, parameterName);
        if (cardId.length() == 9) {
            if (!CardIdUtils.twCardVerify(cardId)) {
                new SystemException(SystemException.TWCARDID_NO_TWCARDID, null, new Object[]{parameterName}).throwEx();
            }
        } else {
            new SystemException(SystemException.TWCARDID_NO_SIZE, null, new Object[]{parameterName}).throwEx();
        }

    }

    /**
     * 验证护照
     * @param
     * @return
     * @author zhy
     * @date 2019/1/4 14:19
     */
    public static void validatePassportCardId(String cardId, String parameterName) {
        Assert.notNull(cardId, parameterName);
        if (cardId.length() == 9) {
            if (!CardIdUtils.passportVerify(cardId)) {
                new SystemException(SystemException.PASSPORTCARDID_NO_TWCARD, null, new Object[]{parameterName}).throwEx();
            }
        } else {
            new SystemException(SystemException.TWCARDID_NO_PASSPORTCARDID, null, new Object[]{parameterName}).throwEx();
        }
    }
}
