package cc.geek8.core.biz.mq.serializer;

import de.ruedigermoeller.serialization.FSTObjectInput;
import io.jafka.message.Message;
import io.jafka.producer.serializer.Decoder;
import cc.geek8.core.biz.mq.service.MQMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

/**
 * 对MQMessage进行反序列化处理
 *
 * @author WanWei
 * @date 2015-10-27 下午2:32:08
 */
public class MQMessageDecoder implements Decoder<MQMessage> {

    private static Logger log = LoggerFactory.getLogger(MQMessageDecoder.class);

    public MQMessageDecoder() {
    }

    /**
     * 反序列化
     */
    public MQMessage toEvent(Message message) {
        //初始化一个data为null的无效消息
        MQMessage result = new MQMessage();
        ByteArrayInputStream bi = null;
        FSTObjectInput in = null;
        if(message == null) {
            return result;
        }
        try {
            ByteBuffer buf = message.payload();
            byte[] b = new byte[buf.remaining()];
            buf.get(b);

            bi = new ByteArrayInputStream(b);
            in = new FSTObjectInput(bi);
            result = (MQMessage)in.readObject(MQMessage.class);
        } catch(Exception e){
            log.error("消息对象反序列化失败，系统无法处理", e);
        } finally {
            try {
                if(in != null){
                    in.close();
                }
                if (bi != null) {
                    bi.close();
                }
            } catch (Exception e) {
                // 发生此异常不会影响系统运行
                log.error("关闭字节流错误", e);
            }
        }

        return result;
    }

}
