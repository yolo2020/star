package cc.geek8.core.biz.mq.service;


import cc.geek8.core.biz.mq.role.MQConsumer;
import cc.geek8.core.biz.mq.role.MQProducer;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息中间件的工厂类
 *
 * @author WanWei
 * @date 2015-10-27 下午3:12:19
 */
public class AplatMQFactory {

    /**
     * 创建指定topic的生产者
     * @param topic
     * @param zkConnect
     * @return
     * @author WanWei
     * @date 2015-10-27 下午4:51:34
     */
    public static MQProducer createProducer(String topic, String zkConnect){
        return new MQProducer(topic, zkConnect);
    }

    /**
     * 创建指定topic的消费者
     * 若需要多个线程处理消息，则会创建同topic且同group的多个消费者
     *
     * @param topic
     * @param zkConnect
     * @param tag
     *		消息过滤标签
     * @param groupId
     * @param handleThread
     * 			处理线程的个数
     * @param listener
     * @return
     * @author WanWei
     * @date 2015-10-28 上午10:44:56
     */
    public static List<MQConsumer> createConsumer(String topic, String zkConnect, String tag, String groupId, int handleThread, IMQMessageListener listener, boolean manualCommit){
        List<MQConsumer> consumers = new ArrayList<MQConsumer>(0);
        for(int i = 0; i < handleThread; i++) {
            MQConsumer consumer = new MQConsumer(topic, zkConnect, groupId, manualCommit);
            if(listener != null){
                consumer.addMessageListener(tag, listener);
            }
            consumers.add(consumer);
        }

        return consumers;
    }
}
