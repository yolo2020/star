package cc.geek8.core.biz.mq.service;
/**
 * 扩展IMQMessageListener 添加listener标识符
 * @author WanWei
 * @date 2015-10-29 上午11:19:06
 */
public class MQEventAdapter implements IMQMessageListener{

    //监听处理者ID,并不要求唯一
    private String listenerId;

    public MQEventAdapter(){

    }

    /**
     * 消息处理方法
     */
    public void onMessage(MQMessage message) {
        //空实现

    }

    public String getListenerId() {
        return listenerId;
    }

    public void setListenerId(String listenerId) {
        this.listenerId = listenerId;
    }

}
