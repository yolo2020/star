package cc.geek8.core.biz.cache.redis;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * 提供java对象在redis中存储的序列化功能
 *
 * @author WanWei
 * @date 2019/3/13 7:19 PM
 **/
public class RedisObjectSerializer implements RedisSerializer<Object> {

    private Converter<Object, byte[]> serializingConverter = new SerializingConverter();

    private Converter<byte[], Object> deserializingConverter = new DeserializingConverter();

    /**
     * 序列化
     *
     * @author WanWei
     * @date 2019-03-13 7:23 PM
     * @param o
     * @return byte[]
     **/
    public byte[] serialize(Object o) throws SerializationException {
        byte[] result = new byte[0];
        if (o == null) {
            return result ;
        }
        result = serializingConverter.convert(o);
        return result;
    }

    /**
     * 反序列化
     *
     * @author WanWei
     * @date 2019-03-13 7:23 PM
     * @param bytes
     * @return java.lang.Object
     **/
    public Object deserialize(byte[] bytes) throws SerializationException {
        Object result = null;
        if (bytes == null || bytes.length == 0) {
            return null ;
        }
        result = deserializingConverter.convert(bytes);
        return result;
    }
}
