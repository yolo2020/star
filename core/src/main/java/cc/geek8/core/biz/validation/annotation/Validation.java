package cc.geek8.core.biz.validation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解，标注被做参数检查的方法
 * 并且会指明用来做具体参数验证的类和方法
 * 配合Spring AOP使用
 *
 * @author WanWei
 * @date 2015-12-16 下午1:29:39
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Validation {

    //用来做具体检查的类
    Class<?> verifyClass();

    //值为true时，代表用来做验证的方法与被检查方法同名
    //值为false时，需要通过verifyMethod指明用哪个方法做检查
    boolean useSameMethod() default true;

    //当useSameMethod = false时，需要指明用哪个方法做检查
    String verifyMethod() default "";
}
