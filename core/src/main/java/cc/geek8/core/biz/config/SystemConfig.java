package cc.geek8.core.biz.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 系统后续扩展追加的一些配置信息
 *
 * @author WanWei
 * @date 2015-11-17 下午1:45:39
 */
@Component
public class SystemConfig {

    @Value("${text_search_cfg}")
    public String textSearchCfg;

    @Value("${dictionary_db_init_onstart}")
    public String initDictionaryToDB;

    @Value("${clean_schedule_job_onstart}")
    public String cleanScheduleJobOnStart;

    public  String getTextSearchCfg() {
        return textSearchCfg;
    }

    public void setTextSearchCfg(String textSearchCfg) {
        textSearchCfg = textSearchCfg;
    }

    public String getInitDictionaryToDB() {
        return initDictionaryToDB;
    }

    public void setInitDictionaryToDB(String initDictionaryToDB) {
        initDictionaryToDB = initDictionaryToDB;
    }

    public String getCleanScheduleJobOnStart() {
        return cleanScheduleJobOnStart;
    }

    public void setCleanScheduleJobOnStart(String cleanScheduleJobOnStart) {
        cleanScheduleJobOnStart = cleanScheduleJobOnStart;
    }
}