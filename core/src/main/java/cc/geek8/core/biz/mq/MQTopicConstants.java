package cc.geek8.core.biz.mq;

/**
 * 定义系统使用的一些topic
 *
 * @author WanWei
 * @date 2015-11-11 下午6:17:22
 */
public interface MQTopicConstants {

    // 工作流模块使用的topic
    String TOPIC_WORKFLOW = "topic-workflow";

    // 系统框架使用的topic,当未指明topic时使用该topic
    String TOPIC_DEFAULT_SYSTEM_TOPIC = "plat-system";

    // 邮件信息使用的topic
    String TOPIC_MAIL = "topic-mail";

    /**
     * 发短信使用的topic
     */
    String TOPIC_SMS = "topic-sms";

    // 通知信息使用的topic
    String TOPIC_NOTIFY = "topic-notify";

    // 转换通知消息用到的topic
    String TOPIC_NOTIFY_SUB_CONVT = "topic-notify-convert";

    // 用来做消息的WEBSOCKET推送的topic
    String TOPIC_MSG_WEBSOCKET = "topic-msg-websocket";

    // 亲子用来做消息的WEBSOCKET推送的topic
    String KIDS_TOPIC_MSG_WEBSOCKET = "kids-websocket";

}
