package cc.geek8.core.biz.exception.filter;


import com.alibaba.dubbo.common.utils.ReflectUtils;
import com.alibaba.dubbo.rpc.*;
import com.alibaba.dubbo.rpc.service.GenericService;
import cc.geek8.core.biz.exception.BaseBizException;
import cc.geek8.core.biz.exception.SystemException;
import cc.geek8.core.utils.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;


/**
 * 扩展dubbo框架的filter，使得dubbo能在调用端和服务端之间传递业务异常
 *
 * @author WanWei
 * @date 2015-12-1 下午12:56:22
 */
public class BizExceptionFilter implements Filter {

    private static Logger log = LoggerFactory.getLogger(BizExceptionFilter.class);

    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        try {
            Result result = invoker.invoke(invocation);
            /*
            if (result.hasException() && GenericService.class != invoker.getInterface()) {
                try {
                    Throwable exception = result.getException();
                    // 日志记录异常
                    log.error(exception.getMessage(), exception);

                    // 如果是checked异常，直接抛出
                    if (!(exception instanceof RuntimeException) && (exception instanceof Exception)) {
                        return result;
                    }

                    // 业务异常
                    if (exception instanceof BaseBizException) {
                        BaseBizException bizException = (BaseBizException) exception;
                        return new RpcResult(
                                new RuntimeException(JSONUtil.getJsonString(bizException.getExceptionSimpleInfo())));
                    }

                    // 使用Assert对入参进行检查时，为通过校验产生的异常
                    if (exception instanceof IllegalArgumentException) {
                        SystemException e = new SystemException(SystemException.RPC_CALL_PATAMETER_ERROR,
                                (Exception) exception);
                        e.getErrorMsg();// 该方法会给异常实体中的errorMsg属性赋值
                        return new RpcResult(new RuntimeException(JSONUtil.getJsonString(e.getExceptionSimpleInfo())));
                    }

                    // 数据库操作异常
                    String exceptionPackgeName = exception.getClass().getPackage().getName();
                    if (exceptionPackgeName.contains("ibatis") || exceptionPackgeName.contains("jdbc")
                            || exceptionPackgeName.contains("postgresql") || exceptionPackgeName.contains("springframework.dao")) {
                        SystemException e = new SystemException(SystemException.DB_OPERATION_ERROR,
                                (Exception) exception);
                        e.getErrorMsg();// 该方法会给异常实体中的errorMsg属性赋值
                        return new RpcResult(new RuntimeException(JSONUtil.getJsonString(e.getExceptionSimpleInfo())));
                    }

                    // 在方法签名上有声明，直接抛出
                    try {
                        Method method = invoker.getInterface().getMethod(invocation.getMethodName(),
                                invocation.getParameterTypes());
                        Class<?>[] exceptionClassses = method.getExceptionTypes();
                        for (Class<?> exceptionClass : exceptionClassses) {
                            if (exception.getClass().equals(exceptionClass)) {
                                return result;
                            }
                        }
                    } catch (NoSuchMethodException e) {
                        return result;
                    }

                    // 未在方法签名上定义的异常，在服务器端打印ERROR日志
                    log.debug(
                            "Got unchecked and undeclared exception which called by "
                                    + RpcContext.getContext().getRemoteHost() + ". service: "
                                    + invoker.getInterface().getName() + ", method: " + invocation.getMethodName()
                                    + ", exception: " + exception.getClass().getName() + ": " + exception.getMessage(),
                            exception);

                    // 异常类和接口类在同一jar包里，直接抛出
                    String serviceFile = ReflectUtils.getCodeBase(invoker.getInterface());
                    String exceptionFile = ReflectUtils.getCodeBase(exception.getClass());
                    if (serviceFile == null || exceptionFile == null || serviceFile.equals(exceptionFile)) {
                        return result;
                    }
                    // 是JDK自带的异常，直接抛出
                    String className = exception.getClass().getName();
                    if (className.startsWith("java.") || className.startsWith("javax.")) {
                        return result;
                    }
                    // 是Dubbo本身的异常，直接抛出
                    if (exception instanceof RpcException) {
                        return result;
                    }

                    // 否则，包装成UnknownException抛给客户端
                    SystemException unknown = new SystemException(SystemException.CATCH_UNKNOWN_EXCEPTION, (Exception) exception);
                    unknown.getErrorMsg();// 该方法会给异常实体中的errorMsg属性赋值
                    return new RpcResult(new RuntimeException(JSONUtil.getJsonString(unknown.getExceptionSimpleInfo())));
                } catch (Throwable e) {
                    log.warn("Fail to ExceptionFilter when called by " + RpcContext.getContext().getRemoteHost()
                            + ". service: " + invoker.getInterface().getName() + ", method: "
                            + invocation.getMethodName() + ", exception: " + e.getClass().getName() + ": "
                            + e.getMessage(), e);
                    return result;
                }
            }
            */
            return result;
        } catch (RuntimeException e) {
            log.error("Got unchecked and undeclared exception which called by "
                    + RpcContext.getContext().getRemoteHost() + ". service: " + invoker.getInterface().getName()
                    + ", method: " + invocation.getMethodName() + ", exception: " + e.getClass().getName() + ": "
                    + e.getMessage(), e);
            throw e;
        }
    }

}
