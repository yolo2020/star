package cc.geek8.core.biz.mq.service;

/**
 * MQ消息监听类
 *
 * @author WanWei
 * @date 2015-10-28 上午8:39:12
 */
public interface IMQMessageListener {

    /**
     * 执行消息处理
     *
     * @param message
     * @author WanWei
     * @date 2015-10-28 上午8:40:43
     */
    public void onMessage(MQMessage message);

}
