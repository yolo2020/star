package cc.geek8.core.biz.cache.redis;

import cc.geek8.core.biz.cache.api.DCacheService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 基于redis的缓存读取实现类
 *
 * @author WanWei
 * @date 2019/3/13 7:41 PM
 **/
@Component
public class RedisCacheServiceImpl implements DCacheService {

//    private static Logger log = LoggerFactory.getLogger(RedisCacheServiceImpl.class);

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public boolean isInCache(String cacheName, String key) {
        return redisTemplate.opsForHash().hasKey(cacheName, key);
    }

    @Override
    public Object get(String cacheName, String key) {
        return redisTemplate.opsForHash().get(cacheName, key);
    }

    @Override
    public void put(String cacheName, String key, Object value) {
        redisTemplate.opsForHash().put(cacheName, key, value);
    }

    @Override
    public void putInCacheExpiry(String cacheName, String key, Object value, long timeSeconds) {
        String realKey = cacheName.concat("@@").concat(key);
        redisTemplate.opsForValue().set(realKey, value, timeSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Object getInCacheExpiry(String cacheName, String key) {
        String realKey = cacheName.concat("@@").concat(key);
        return redisTemplate.opsForValue().get(realKey);
    }

    @Override
    public void remove(String cacheName, String key) {
        redisTemplate.opsForHash().delete(cacheName, key);
    }

    @Override
    public void remove(String cacheName) {
        redisTemplate.delete(cacheName);
    }

    @Override
    public List<Object> getKeys(String cacheName) {
        return new ArrayList<>(redisTemplate.opsForHash().keys(cacheName));
    }
}
