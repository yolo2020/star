package cc.geek8.core.biz.mq;


/**
 * 系统里用到的一些MQ标签
 *
 * @author WanWei
 * @date 2015-11-11 下午6:21:38
 */
public interface MQTagConstants {

    //业务操作日志的标签
    String EVENT_TAG_OPERATION = "tagOperation";

    //发送邮件的标签
    String EVENT_TAG_MAIL = "tagMail";

    // 添加索引的标签
    String ADD_INDEX = "addGlobalSearchIndex";

    // 添加索引的标签单个
    String ADD_SINGLE_INDEX = "addSingleGlobalSearchIndex";

    // 更新索引的标签
    String UPDATE_INDEX = "updateGlobalSearchIndex";

    // 更新索引的标签
    String UPDATE_SINGLE_INDEX = "updateSingleGlobalSearchIndex";

    // 删除索引的标签
    String DELETE_INDEX = "deleteGlobalSearchIndex";

}
